package pack_teclado_ergonomico;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import generaPoblacion.GeneraPoblacion;
import librerias_componentes.*;


import pack_teclado_ergonomico.*;
import pack_teclado_ergonomico.*;


@SuppressWarnings("unused")
public class Parametros {
	String posicion = "11,23,13,3,14,15,16,9,17,18,19,20,21,22,23,24,17,18,19,20,21,22,23,24,22,26,21";
	Estructura_teclado ETE = new Estructura_teclado();
	Map<String, Integer> pulsacionesMI_D1 = new HashMap<String, Integer>();
	Map<String, Integer> pulsacionesMI_D2 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID3 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID4 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID5 = new HashMap<String, Integer>();
	
	// Declaración variables mano dwerecha
		Map<String, Integer> pulsacionesMD_D1 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D2 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D3 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D4 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D5 = new HashMap<String, Integer>();
	estadísticas_texto ET = new estadísticas_texto();
	ArrayList <String> contenido_archivo = new ArrayList <String>();
	private ArrayList <String> contenido_archivocodified = new ArrayList <String>();
	ArrayList <String> titulo_archivo = new ArrayList <String >();
	ArrayList <String> readcontenido_archivo = new ArrayList <String>();
	private String ConfigFile;

	private String FileContent;
	private File[] listaArchivos;
	private String[] regexTitulo;
	String Lista_archivosSeparado;
	private String file_name;
	private String Contenido_archivo;
    private String PATH_TRABAJO;
    public String FOLDER;
    private String PATH_SALIDA;
    private int TAM_GRAMA;
    MyListArgs Param;
    IOBinFile BinFile;
    ManejoArchivos IO;
    StringBuffer Report;

    public Parametros(String []Args) {
       	IO = new ManejoArchivos();
        BinFile = new IOBinFile();

        Param = new MyListArgs(Args);

        ConfigFile = Param.ValueArgsAsString("-CONFIG", "");//Ruta donde se van a crear los individuos
        if (!ConfigFile.equals("")) {
        	PATH_TRABAJO	= Param.ValueArgsAsString   ("-PATH_TRABAJO", "C:\\Users\\Nestor\\Desktop\\teclado-ergonomico\\PATH_TRABAJO\\"    );
            FOLDER			= Param.ValueArgsAsString   ("-FOLDER"			, ""    );
            PATH_SALIDA     = Param.ValueArgsAsString   ("-PATH_SALIDA"		, "C:\\Users\\nesto\\OneDrive\\Documentos\\dataset_formateado"    );
            TAM_GRAMA       = Param.ValueArgsAsInteger  ("-TAM_GRAMA"		, 0     );
        }
        
    
	
	}
    //@SuppressWarnings("static-access")
	public void Run () throws IOException {
		GeneraPoblacion GA = new GeneraPoblacion();
		estadísticas_texto et = new estadísticas_texto();
 		et.mapeo_Caracter();
		
 		GA.genera_Individuos(PATH_TRABAJO, posicion);
		//et.asigna_teclaDedo();
		Manejo_archivos ma = new Manejo_archivos();
   //ASIGNACIÓN DE TECLAS A CADa DEDO DE LA MANO DERECHA--------------
		pulsacionesMD_D1 = ET.asigna_teclas_M_DERECHA();
		pulsacionesMI_D1 =ET.asigna_teclaDedo_M_IZQUIERDA();
		
	pulsacionesMD_D1 =ET.pulsacionesMD_D1;
	pulsacionesMD_D2 = ET.pulsacionesMD_D2;	
	pulsacionesMD_D3 = ET.pulsacionesMD_D3;
	pulsacionesMD_D4 = ET.pulsacionesMD_D4;
	pulsacionesMD_D5 = ET.pulsacionesMD_D5;

	pulsacionesMI_D1 = ET.pulsacionesMI_D1;
	pulsacionesMI_D2 =ET.pulsacionesMI_D2;
	pulsaciones_MID3 =ET.pulsaciones_MID3;
	pulsaciones_MID4 = ET.pulsaciones_MID4;
	pulsaciones_MID5 = ET.pulsaciones_MID5;
		
	
		
// recorrer los valores de cada dedo de la mano DERECHA------------
		
	for (String i : pulsacionesMD_D1.keySet()) {
		  System.out.println("num_teclas:  " + i + " DEDO  : " + pulsacionesMD_D1.get(i));
		  ETE.Estructura_tecladoAlfanumerico(i);
		}
	for (String i : pulsacionesMD_D2.keySet()) {
		  System.out.println("num_teclas:  " + i + " DEDO  : " + pulsacionesMD_D2.get(i));
		}
	
	for (String i : pulsacionesMD_D3.keySet()) {
		
		  System.out.println("num_teclas: " + i + " DEDO: " + pulsacionesMD_D3.get(i));

		}
	for (String i : pulsacionesMD_D4.keySet()) {
		  System.out.println("DEDO: " + i + " num_teclas : " + pulsacionesMD_D4.get(i));
		}	
	for (String i : pulsacionesMD_D5.keySet()) {
		  System.out.println("num_teclas: " + i + " DEDO : " + pulsacionesMD_D5.get(i));
		
		}	
	for (String i : pulsacionesMI_D1.keySet()) {
		// recorrer los valores de cada dedo de la mano IZQUIERDA------------
		  System.out.println("num_teclas: " + i + " DEDO : " + pulsacionesMI_D1.get(i));
		}	
	for (String i : pulsacionesMI_D2.keySet()) {
		  System.out.println("value: " + i + " num_teclas : " + pulsacionesMI_D2.get(i));
		}
	for (String i : pulsaciones_MID3.keySet()) {
		  System.out.println("value: " + i + " D3 : " + pulsaciones_MID3.get(i));
		}
	for (String i : pulsaciones_MID4.keySet()) {
		  System.out.println("value: " + i + " D4 : " + pulsaciones_MID4.get(i));
		}

	for (String i : pulsaciones_MID5.keySet()) {
		  System.out.println("num_teclas: " + i + " D5 : " + pulsaciones_MID5.get(i));
		}


    	ma.read_multipleFilesModified(listaArchivos);
    	//listaArchivos =ma.getLista_arcchivos();
    	//file_name = ma.nombre_archivo();
    	//file_name = ma.getfile_name();
    	//ma.writeToTextFile(contenido_archivo);
    	


    
		}
    public String setPathTrabajo(String PATH_TRABAJO) {
    	return this.PATH_TRABAJO = PATH_TRABAJO;
    }
    
    public String getPathTrabajo() {
		
		return PATH_TRABAJO;
		
	}
    	
    }


