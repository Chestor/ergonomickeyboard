 package pack_teclado_ergonomico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class estad�sticas_texto {
	Estructura_teclado ET= new Estructura_teclado();
	
	// Declaraci�n variables mano Izquierda
	Map<String, Integer> pulsacionesMI_D1 = new HashMap<String, Integer>();
	Map<String, Integer> pulsacionesMI_D2 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID3 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID4 = new HashMap<String, Integer>();
	Map<String, Integer> pulsaciones_MID5 = new HashMap<String, Integer>();
	// Declaraci�n variables mano dwerecha
		Map<String, Integer> pulsacionesMD_D1 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D2 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D3 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D4 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMD_D5 = new HashMap<String, Integer>();
	
	
	private String[] posicion;
	private String texto_entrada = "holamundo";
	FormatText FT = new FormatText();
	String cadenaSplit;
	



 
	
	
	private String []alfabetost;
	private String texto_formateadoalfa;
	private String texto_formateado_po;

	
	//private String[] Alfabeto; 
	
	private ArrayList<Integer> Texto_vectorizado = new ArrayList<Integer>();
	
	public  Map < String, Integer> asigna_teclas_M_DERECHA() {
		//M�todo para asignar teclas correspondientes a cada dedo de la mano Derecha--------------------------------------------------------------------------------------------------------

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("4,5,14,15,24,25", 2); 
		pulsacionesMD_D3.put("8,18,28", 3);
		pulsacionesMD_D4.put("9,19,29", 4);
		pulsacionesMD_D5.put("10,20,30", 5);
		// termina asignacion de teclas de  la mano derecha--------
		
	
			return pulsacionesMD_D1;	
	}
	public  Map < String, Integer> asigna_teclaDedo_MD_2() {
		pulsacionesMD_D1.put( "55,56", 1);
		pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 4);
		
	
			return pulsacionesMD_D2;
			
	}
	public  Map < String, Integer> asigna_teclaDedo_MD_3() {

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
		
	
			return pulsacionesMD_D3;
			
	}
	public  Map < String, Integer> asigna_teclaDedo_MD_4() {

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
		
	
			return pulsacionesMD_D4;
			
	}
	public  Map < String, Integer> asigna_teclaDedo_MD_5() {

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("4,5,14,15,24,25", 2); 
		pulsacionesMD_D3.put("8,18,28,", 3);
		pulsacionesMD_D4.put("9,19,29", 4);
		pulsacionesMD_D5.put("10,20,", 5);
		// Termina asignacion de teclas por dedo de la mano derecha ----
	
			return pulsacionesMD_D5;
			
	}//M�todo para asignar teclas correspondientes a cada dedo de la mano Izquierda---------------------
	public  Map < String, Integer> asigna_teclaDedo_MI_1() {


	
			return pulsacionesMI_D1;
			
	}
	public  Map < String, Integer> asigna_teclaDedo_MI_2() {

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("6,7,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
		// Termina asignacion de teclas por dedo de la mano derecha ----
	
			return pulsacionesMI_D2;
			
	}
	public  Map < String, Integer> asigna_teclaDedo_M_IZQUIERDA() {
//M�todo para asignar teclas a dedos de la mano izquierda--------------------------------------------------------------------------------------------------------------------------
		pulsacionesMI_D1.put( "56", 1);
		pulsacionesMI_D2.put("4,5,14,15,24,25", 2);
		pulsaciones_MID3.put("3,13,23", 3);
		pulsaciones_MID4.put("2,12,22", 4);
		pulsaciones_MID5.put("1,11,21", 5);
		// Termina asignacion de teclas por dedo de la mano derecha ----
	
			return pulsaciones_MID3;
	}
	public  Map < String, Integer> asigna_teclaDedo_MI_4() {

		pulsacionesMD_D1.put( "6,7,16,17,26,27", 1);
		pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
		// Termina asignacion de teclas por dedo de la mano derecha ----
	
			return pulsaciones_MID4;
	}
	public  Map < String, Integer> asigna_teclaDedo_MI_5() {

		pulsacionesMD_D1.put( "56", 1);
		pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
		pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
		pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
		pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
		// Termina asignacion de teclas por dedo de la mano derecha ----
	
			return pulsaciones_MID5;
	}// Termina asignacion de teclas por dedo de la mano Izquierda ----

	
	





public void mapeo_Caracter() {
	
	StringBuffer sb = new StringBuffer();
	//cadena_split = cadenaPrueba.split(" ");
	
	Map<String, String> mapDedo_Caracter = new HashMap<String, String>();
	Map<String, Integer> mapeoDedo_Caracter = new HashMap<String, Integer>();
	mapDedo_Caracter.put("a,b,c,d,e,f,g,h,i,j,k,l,m,n,�,o,p,q,r,s,t,u,v,w,x,y,z", "11,23,13,3,14,15,16,9,17,18,19,20,21,22,23,24,17,18,19,20,21,22,23,24,22,26,21");
	
	mapeoDedo_Caracter.put("a",11);
	mapeoDedo_Caracter.put("b",25);
	mapeoDedo_Caracter.put("c",23);
	mapeoDedo_Caracter.put("d",13);
	mapeoDedo_Caracter.put("e",14);
	mapeoDedo_Caracter.put("f",15);
	mapeoDedo_Caracter.put("g",16);
	mapeoDedo_Caracter.put("h",9);
	mapeoDedo_Caracter.put("i",8);
	mapeoDedo_Caracter.put("j",17);
	mapeoDedo_Caracter.put("k",18);
	mapeoDedo_Caracter.put("l",19);
	mapeoDedo_Caracter.put("m",27);
	mapeoDedo_Caracter.put("n",26);
	mapeoDedo_Caracter.put("�",20);
	mapeoDedo_Caracter.put("o",9);
	mapeoDedo_Caracter.put("p",10);
	mapeoDedo_Caracter.put("q",1);
	mapeoDedo_Caracter.put("r",4);
	mapeoDedo_Caracter.put("s",12);
	mapeoDedo_Caracter.put("t",5);
	mapeoDedo_Caracter.put("u",7);
	mapeoDedo_Caracter.put("v",25);
	mapeoDedo_Caracter.put("w",2);
	mapeoDedo_Caracter.put("x",23);
	mapeoDedo_Caracter.put("y",6);
	mapeoDedo_Caracter.put("z",21);



	Iterator<String> ite = mapDedo_Caracter.keySet().iterator();
	 


}


public void asigna_teclaDedo_MI() {
	pulsacionesMI_D1.put( "56", 1);
	pulsacionesMI_D2.put("5,6,20,21,33,34,46,47", 2);
	pulsaciones_MID3.put("3,8,18,23,31,36,44,49", 3);
	pulsaciones_MID4.put("3,8,18,23,31,36,44,49", 4);
	pulsaciones_MID5.put("1,2,11,12,13,14,15,16,17,24,25,26,27,28,29,30,39,40,41,42,43,51,52,53,54", 5);	
}
	

	



			

			
			



	



}









