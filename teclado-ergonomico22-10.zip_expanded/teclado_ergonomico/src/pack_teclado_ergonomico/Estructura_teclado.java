package pack_teclado_ergonomico;


import java.util.HashMap;
import java.util.Map;

public class Estructura_teclado {
	// Declaraci�n variables mano Izquierda
		Map<String, Integer> pulsacionesMI_D1 = new HashMap<String, Integer>();
		Map<String, Integer> pulsacionesMI_D2 = new HashMap<String, Integer>();
		Map<String, Integer> pulsaciones_MID3 = new HashMap<String, Integer>();
		Map<String, Integer> pulsaciones_MID4 = new HashMap<String, Integer>();
		Map<String, Integer> pulsaciones_MID5 = new HashMap<String, Integer>();
		// Declaraci�n variables mano dwerecha
			Map<String, Integer> pulsacionesMD_D1 = new HashMap<String, Integer>();
			Map<String, Integer> pulsacionesMD_D2 = new HashMap<String, Integer>();
			Map<String, Integer> pulsacionesMD_D3 = new HashMap<String, Integer>();
			Map<String, Integer> pulsacionesMD_D4 = new HashMap<String, Integer>();
			Map<String, Integer> pulsacionesMD_D5 = new HashMap<String, Integer>();
	
	


	public String Estructura_tecladoAlfanumerico(String Numtecla_pulsada) {
		
		Map <Integer, Integer> Coordenadas= new HashMap<Integer, Integer>();// el vecor de coordenadas se componen de la fila ónde se pulsó la tecla y el número de veces que esta se pulsó 
		int num_fila=0;
		int x =4;
		  
		int freq_pulsaciones=0;
		String Fila = "";
		String Fila_Separated[];
		String tecla_pulsada[];
		String Fila_1= "1,2,3,4,5,6,7,8,9,10,56";
		 String Fila_2= "11,12,13,14,15,16,17,18,19.20";
		 String Fila_3= "21,22,23,24,25,26,27";
		 Fila_Separated = Fila_1.split(",");
		tecla_pulsada = Numtecla_pulsada.split(",");
		for (int i=0; i<tecla_pulsada.length; i++) {
			for( int  j=0; j<Fila_Separated.length; j++) {
				if (tecla_pulsada[i].equals(Fila_Separated[j])){
					System.out.println("Entre Fila 1");
					 freq_pulsaciones+=1;
					 Fila = "Fila_1";
					 num_fila =1;
					 
					
				}
				
				
			}
			
			
		}
		switch(Fila) {
		case "Fila_1":
			
			System.out.println("Fila 1");
			break;
		case "Fila_2":
			System.out.println("Fila 2");
		case "Fila_3":
			System.out.println("Fila 3");
		case "Fila_4":
			System.out.println("Fila 4");
		case "Fila_5":
			System.out.println("Fila 5");
	}
		Coordenadas.put(num_fila, freq_pulsaciones);
		return null;

		 }
//M�todo para asignar teclas correspondientes a cada dedo de la mano Izquierda---------------------
public  Map < String, Integer> asigna_teclaDedo_MI_1() {



		return pulsacionesMI_D1;
		
}
public  Map < String, Integer> asigna_teclaDedo_MI_2() {

	pulsacionesMD_D1.put( "56", 1);
	pulsacionesMD_D2.put("6,7,20,22,23,34,35,36,48,49", 2); 
	pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
	pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
	pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
	// Termina asignacion de teclas por dedo de la mano derecha ----

		return pulsacionesMI_D2;
		
}
public  Map < String, Integer> asigna_teclaDedo_M_IZQUIERDA() {
//M�todo para asignar teclas a dedos de la mano izquierda--------------------------------------------------------------------------------------------------------------------------
	pulsacionesMI_D1.put( "56", 1);
	pulsacionesMI_D2.put("4,5,14,15,24,25", 2);
	pulsaciones_MID3.put("3,13,23", 3);
	pulsaciones_MID4.put("2,12,22", 4);
	pulsaciones_MID5.put("1,11,21", 5);
	// Termina asignacion de teclas por dedo de la mano derecha ----

		return pulsaciones_MID3;
}
public  Map < String, Integer> asigna_teclas_M_DERECHA() {
	//M�todo para asignar teclas correspondientes a cada dedo de la mano Derecha--------------------------------------------------------------------------------------------------------

	pulsacionesMD_D1.put( "56", 1);
	pulsacionesMD_D2.put("4,5,14,15,24,25", 2); 
	pulsacionesMD_D3.put("8,18,28", 3);
	pulsacionesMD_D4.put("9,19,29", 4);
	pulsacionesMD_D5.put("10,20,30", 5);
	// termina asignacion de teclas de  la mano derecha--------
	

		return pulsacionesMD_D1;	
}
public  Map < String, Integer> asigna_teclaDedo_MI_4() {

	pulsacionesMD_D1.put( "6,7,16,17,26,27", 1);
	pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
	pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
	pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
	pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
	// Termina asignacion de teclas por dedo de la mano derecha ----

		return pulsaciones_MID4;
}
public  Map < String, Integer> asigna_teclaDedo_MI_5() {

	pulsacionesMD_D1.put( "56", 1);
	pulsacionesMD_D2.put("7,8,20,22,23,34,35,36,48,49", 2); 
	pulsacionesMD_D3.put(",9,19,24,32,37,49", 3);
	pulsacionesMD_D4.put(",18,25,23,31,38,44,51,57,64", 4);
	pulsacionesMD_D5.put("1,2,11,12,13,14,15,16,17,26,27,28,29,30,29,30,39,40,41", 5);
	// Termina asignacion de teclas por dedo de la mano derecha ----

		return pulsaciones_MID5;
}// Termina asignacion de teclas por dedo de la mano Izquierda ----

		
	}



