package pack_teclado_ergonomico;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;



public class Main_tecladoErgonomico {
	static String PATH;
	
	String TECLADO_OPTIMIZAR;

	
	
	


	public Main_tecladoErgonomico() {

	
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException {
		

	Parametros param = new Parametros(args);
		String Path_tecladoGenerado;
		String idioma ="";
		String pathTO;
		String pathEstadisticas="";
		String dataset ="";
		String mano_utilizar="";
		String pathPoblacion ="";
		int numManos;
		String dedosDisponibles;
		String teclado_toOptimizar;
		PARAM load = new PARAM(args);
		PATH = load.getPATH_TRABAJO();
		//seccion para obtener todos los parámetros de trabajo y criterios ergonómicos //
		teclado_toOptimizar = Parametros.getTECLADO_OPTIMIZAR();
		Path_tecladoGenerado = PARAM.getTECLADOS_GENERADOS();
		numManos = Parametros.getNUM_MANOS();
		mano_utilizar = Parametros.getMANO_UTILIZAR();
		dedosDisponibles = Parametros.getDEDOS_DISPONIBLES();
		idioma = Parametros.getIdioma();
		dataset = Parametros.getDATASET();
		pathPoblacion = Parametros.getPATH_POP();
		pathEstadisticas = Parametros.getPATH_ESTADISTICAS();
		//System.out.println(PATH);
		Corre_Proyecto Run_Project = new Corre_Proyecto();

		Run_Project.Ejecuta_proyecto(PATH, teclado_toOptimizar, Path_tecladoGenerado, numManos, dedosDisponibles, idioma, mano_utilizar, dataset,pathPoblacion, pathEstadisticas);
		
		
		

		
		
		
		
	

 	}
	
}
