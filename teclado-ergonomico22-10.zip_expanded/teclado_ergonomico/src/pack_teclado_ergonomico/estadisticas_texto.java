 package pack_teclado_ergonomico;



import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.print.attribute.standard.PrinterLocation;


public class estadisticas_texto {
	private String textFile;
	private boolean cruza=false; 
	private ArrayList<Integer>Hijo2g= new ArrayList<Integer>();
	private ArrayList<Integer>Hijo1g= new ArrayList<Integer>();
	private ArrayList<Integer>Hijo2= new ArrayList<Integer>();
	private ArrayList<Integer>Hijo1= new ArrayList<Integer>();
	private int posicioninicialx = 0;
	private ArrayList<Integer>Padre1= new ArrayList<Integer>();
	private ArrayList<Integer>Padre2= new ArrayList<Integer>();
	//private int numTeclado=0;
	private double probabilidadMutacion=9;
	private int posicionInicialy = 0;
	private String Folder="";
	private String pathpoblacionglobal="";
private double porcentajeManoMD;
private double porcentajeManoMI;
private double aptitud;
private double aptitudmd;
private double aptitudmi;
private String nombreindHijo="";
private int numHijo;


ArrayList<Double> aptitudbh;
Manejo_archivos Ma = new Manejo_archivos();
	int validacion;
	int nIndividuo;
	String nombreTeclado="";
 
	  

	 
	TreeMap<Integer, Double> fingerPorcentage = new TreeMap<Integer, Double>();
	HashMap<Integer, Double> fingerPorcentageMD = new HashMap<Integer, Double>();
	TreeMap<Integer, Double> porcentajeDedosMI = new TreeMap<Integer, Double>();
	TreeMap<Integer, Double> porcentajeDedosMD = new TreeMap<Integer, Double>();
	Map<String, Integer> fingerStatisticsMD = new HashMap<String, Integer>();
	Map<String, Integer> fingerStatisticsMI = new HashMap<String, Integer>();
	Map<String, Integer> estadisticaMI = new HashMap<String, Integer>();
	Map<String, Integer>tecladoMap = new LinkedHashMap<String, Integer>();
	Map<String, Integer>MaptecladoAleatorio = new LinkedHashMap<String, Integer>();
	Manejo_archivos MA = new Manejo_archivos();
	HashMap<Integer, Double> fingerPorcentagManoMD = new HashMap<Integer, Double>();
	
	
	int numTeclado;
	int numGeneracion=0;
	Map<String, Integer> estadisticaMD = new HashMap<String, Integer>();
	Map<String, Integer>textStatistics = new HashMap<String, Integer>();
Map<String, Integer>tecnicaDigitacionMD = new LinkedHashMap<String, Integer> ();
Map<String, Integer>tecnicaDigitacionMI = new LinkedHashMap<String, Integer> ();

String Alfabeto = "abcdefghijklmnñopqrstuvwxyz";


public Map<String, Integer> cargaTecladoOptimizar (String teclado_optimizar ) {

	//pulsacionesDedo(textFile, tecladoAleatorio, tecnicaDigitacionMD, tecnicaDigitacionMI);
	//tecladoAleatorio= "C:\\Users\\Chestor\\Documents\\proyecto_versiones\\pathTrabajo\\tecladosGenerados\\Teclado Generado1.txt";
	int numFila=0;
	ArrayList<String>Teclado = new ArrayList<String> ();
	String []filasTeclado;

	try {
		List<String> allLines = Files.readAllLines(Paths.get(teclado_optimizar));

		for (String line : allLines) {
			numFila++;
			
			//System.out.println(line);
			filasTeclado = line.split("[\\(||\\)]");
			System.out.println(line);
			
		
			Teclado.add(line);
			tecladoMap.put(line, numFila);
		}
	} catch (IOException e) {
		e.printStackTrace();
	}


	return tecladoMap;

}

public Map<String, Integer> tecnica_DigitacionMI () {
	//Técnica de digitación de la mano izquierda
		tecnicaDigitacionMI.put(" ", 1);// Dedo pulgar
	tecnicaDigitacionMI.put("5,t,g,b,4,r,f,v", 2);// Dedo índice MI
	tecnicaDigitacionMI.put("3,e,d,c", 3);// Dedo medio MI
	tecnicaDigitacionMI.put("2,w,s,X", 4);// Dedo anular MI
	tecnicaDigitacionMI.put("1,q@,a,z", 5);// Dedo meñique MI
	return tecnicaDigitacionMI;
}
public Map<String, Integer> tecnica_Digitacion_MD () {
	//Técnica de digitación de la mano derecha
		tecnicaDigitacionMD.put(" ", 1);// Dedo pulgar
	tecnicaDigitacionMD.put("6,y,h,n,7,u,j,m", 2);// Dedo índice
	tecnicaDigitacionMD.put("8,i,k,,", 3);// Dedo medio
	tecnicaDigitacionMD.put("9,o,l,.", 4);// Dedo anular
	tecnicaDigitacionMD.put("0,p,ñ;,.,;,:", 5);// Dedo meñique
	return tecnicaDigitacionMD;
}

@SuppressWarnings("null")
public void generaTeclado (ArrayList<Integer>Individuos, Map<String, Integer>TecladoMap, String pathPoblacion, String PathTecladoGenerado, String folder, Map<String, Integer> tablaIndividuo) throws IOException {
	System.out.println("El tamaño del individuo es: " + Individuos.size());
	ArrayList<String> Allteclado = new ArrayList<String>();
	 ArrayList<ArrayList<String>> tecladoMatriz = new ArrayList<ArrayList<String>>();
	numTeclado++;
	nombreTeclado = "teclado_Generado "+numTeclado;
	int filaAct=0;
	int numgen = 0;
	int numcolumna= 0;
	int numFilas=4;
String fileContent = "";
	boolean decimal = false;
	int numColumnas=40;
	int totalColumnas=10;
String contenidoFilas="";
	String[] contenidoFila = null;
	String tecladoGenerado = null;
	String filasTeclado = "";
	double distancia = 0.0;
String []tecladoCompleto;
int numCromosoma = 0;
for (String clave:TecladoMap.keySet()) { 

	contenidoFila= clave.split(",");
	


	
	}

System.out.println("El numero de teclas del teclado es: "+Allteclado.size());


	
		//for (int i =0; i< numFilas; i++) {
		

			
		
	for (String clavete:TecladoMap.keySet()) {
				while(numcolumna<= contenidoFila.length) {
					
					for(Entry<String, Integer> entry: tablaIndividuo.entrySet()) {
						
							int numFila = TecladoMap.get(clavete);
							//System.out.println("Clave: " + clave + ", valor: " + numFila);
							contenidoFila= clavete.split(",");
				
				numgen = Individuos.get(numCromosoma); 
				if( numgen >=tablaIndividuo.size()){
					numgen = numgen-1;
				}
			      // if give value is equal to value from entry
			      // print the corresponding key
				
			      if(entry.getValue() == numgen) {
			    	  numCromosoma+=1;
			    	  if (numCromosoma==1) {
			    		  fileContent+="[";
			    	  }
			    	  if (numCromosoma>=40) {
			    		  MA.writeTecladoAleatorio(fileContent, PathTecladoGenerado, nombreTeclado, folder);
			    			 return;
						
						   }
			    	 
			    	 numcolumna++;
			    	 
			    	 System.out.println("la tecla del gen " + numgen + " es " + entry.getKey());
				        fileContent+=","+entry.getKey();
			    	 if (numcolumna>= contenidoFila.length-1) {
			    		 filaAct++;
			    		 
			    		 System.out.println( "<------ El contenido de la fila "+ filaAct+ " fué "+ fileContent);
							MA.writeTecladoAleatorio(fileContent, PathTecladoGenerado, nombreTeclado, folder);
						  // System.out.println( "<------ El contenido de la fila "+ filaAct+ " fué "+ fileContent);
			    		 fileContent+="]";
							fileContent+="\n";
							numcolumna=0;
							   
							 //  fileContent+="]";
								//fileContent+="\n";
							   //filaAct++;
							 
						
							 //  numFila++;
			    	 }
								
			    		 //numFila++;
			    		 //numcolumna=0;
			    		 
			        
			        //break;
			      }
					  
			     
					
			}
			 
		    
	
				
			
				
 		  
		   
		    	

	
					
					
				
		
		    
		   
					
				
					
					

					
					//j--;	
				
		}
		//fileContent="";
	
	
	
		
				
			
				
				
				
			}

			
			

		}
		
		
		
			



public double EstadisticasGeneralesTA (String textFile, Map<String, Integer> tecladoMap, String Filecontent, String nombreTeclado,Map<String, Integer>tecMD, Map<String, Integer> tecMI, int numManos, String dedosDisponibles) {
	ArrayList<Double>DistanciaAM = new ArrayList<Double>();
	
	int frecuenciad=0;
	StringBuffer sb = new StringBuffer();
	int x1;
	int criterioErgonomico;
	criterioErgonomico = numManos* dedosDisponibles.length();
	int x2;
	int y1;
	int y2;
	String contenidoFilaarr[];
	String contenidoFilast = null;
	int posicionInicialy;
	int fPulsaciones=0;
	int posicionionicialx;
	int numColumnas= 10;
	int numFilas=4;
	int PI = 0;
	Map<String, Integer> estadisticasRK = new LinkedHashMap<String, Integer>();
	posicionionicialx= (int)(Math.random()*numColumnas);
	posicionInicialy= (int)(Math.random()*numFilas);
	if (numManos==1) {
		PI=posicionionicialx- posicionInicialy;
		if (PI<1) {
			PI = PI*PI;
		}
	}else if (numManos>1) {
		PI=0;
		
	}

	for (String clave:tecladoMap.keySet()) {
		for (Integer clavefd:fingerPorcentageMD.keySet()) {
			
		}
				contenidoFilaarr = clave.split(",");
		 for (int i=PI; i<contenidoFilaarr.length; i++) {
			    sb.append(contenidoFilaarr[i]);	
		 }
			    contenidoFilast = sb.toString();
			    
			    
			    
	}

	int numFila = 0;
	double distancia = 0;
	double distanciaAM = 0;
int x=0;
int y;
	
	
	
		for (String clave:tecladoMap.keySet()) {
			numFila++;
		    int valor = tecladoMap.get(clave);
		    contenidoFilaarr = clave.split(",");
		   
		
		   
		    for (int cf=0; cf<contenidoFilast.length();cf++) {
		    	for (int t=0;t<Filecontent.length();t++) {
		    if (Filecontent.charAt(t)== contenidoFilast.charAt(cf)) {
		    	fPulsaciones+=3;
		    	
		    	x1=cf;
		    	y1 = valor;
		    	y2 = numFila;
		    	x2 =t;
		    	distanciaAM = Math.pow(x1, x2);
		    	distanciaAM = distanciaAM * Math.pow(y1, y2);
		    	distanciaAM = Math.sqrt(distanciaAM);
		    	distanciaAM = distanciaAM *criterioErgonomico;
		    	
	
		    	frecuenciad++;
		    	x= cf- fPulsaciones;
		    	y = t - x;
		    	
		    
		    	
		    	distancia = y*8;
		    	distancia = Math.floor(distancia);
		    	x=cf;
		    	
		    	
		    	distancia = Math.sqrt(distancia); 
		    	if (numManos>1) {
		    		distancia = distancia/2;
		    	}
		    	
		    	estadisticasRK.put(contenidoFilast.charAt(cf)+" ", frecuenciad);
		    	
		    }
		    
		    }frecuenciad=0; 
		    
		   
		}
		
	}frecuenciad=0; 
	    	  	
	
 
	
	

	
	    	System.out.println("Imprimiendo estadisticas de teclado Generado");
	    	for (String clave:estadisticasRK.keySet()) {
	    	    int valor = estadisticasRK.get(clave);
	    	    System.out.println("caracter: " + clave + ", frecuencia: " + valor);
	    	    System.out.println("La distancia del teclado: "+ nombreTeclado +" es : " +distancia);
	    	}
	    	if (numManos>1) {
	    		
	    	}
	    	distancia = distancia/2;
	    	DistanciaAM.add(distancia);
	    	return distancia;
	    
	}


public Map<String, Integer> putEstadisticasMI( String texto, Map<String, Integer> tecnicaDigitacionMI, Map<String, Integer> teclado) {

	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	

	int freecuencia = 0;
	for (String clave:teclado.keySet()) {
	    int valor = teclado.get(clave);
	   // System.out.println("contenidoFila: " + clave + ", Dedo encargado: " + valor);
	    contenidoFila = clave.split(",");
	    for (int i =1;i<contenidoFila.length-1;i++) {
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<texto.length(); t++) {
	    		if (texto.charAt(t)== contenidoFila[i].charAt(cc)) {
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMI.put( contenidoFila[i],frecuenciaPulsacion);
	    			
	    			
	    		}
	    		
	    	}
	    		frecuenciaPulsacion=0;
	    	}
	    	
	    	
	    	
	    }
	    
	}
	System.out.println("Imprimiendo estadisticas de la mano izquierda");
	for (String clave:estadisticaMI.keySet()) {
	    int valor = estadisticaMI.get(clave);
	    System.out.println("caracter: " + clave + ", frecuencia: " + valor);
	}
	return estadisticaMI;
	

}
public void pulsacionesMD(Map<String, Integer> tecnicaDigitacionMD, String texto) {
	//System.out.println("Imprimiendo estadisticas de la mano derecha");
	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	
	int frecuencia = 0;
	for (String clave:tecnicaDigitacionMD.keySet()) {
	    int valor = tecnicaDigitacionMD.get(clave);
	    System.out.println("contenidoFila: " + clave + ", numDedo: " + valor);
	    contenidoFila = clave.split(",");
	    for (int i =0;i<contenidoFila.length;i++) {
	    	
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<texto.length(); t++) {
	    		if (texto.charAt(t)== contenidoFila[i].charAt(cc)) {
	    		fingerStatisticsMD.put(contenidoFila[i]+"", valor);
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMD.put( contenidoFila[i],frecuenciaPulsacion);
	    			
	    			
	    		}
	    		
	    	}
	    	}
	    	
	    	
	    	
	    }
	}	
	System.out.println("\n Imprimiendo estadisticas por dedo de la mano derecha");
	for (String clave:fingerStatisticsMD.keySet()) {
	    int valor = fingerStatisticsMD.get(clave);
	    System.out.println("caracter: " + clave + ", dedo que pulsa: " + valor);
	}
	
}
public void evaluaTecladosAleatorios(String texto, String pathTG,Map<String, Integer> teclado, Map<String, Integer> textStatistics, Map<String, Integer>tecMD, Map<String, Integer> tecMI,Map<String, Integer> fingerMD,Map<String, Integer> teclasPulsadasBH, String pathEstadisticas, int numManos, String dedosDisponibles, String mano_utilizar, Map<String, Integer>teclasPulsadasLH) throws IOException {
	ArrayList<Double>DistanciaMDal = new ArrayList<Double>();
	

	ArrayList<Double>DistanciaMIal = new ArrayList<Double>();

	ArrayList<Double>DistanciaMI = new ArrayList<Double>();
	ArrayList<Double> distanciaAM =new ArrayList<Double>();
	ArrayList<Double> aptitudes =new ArrayList<Double>();
	funcionAptitud FA = new funcionAptitud();
	String tecladoAleatorio;
    String carpeta = "";
    String aptitudst = "";
int numTeclado=0;
	double distanciamd=0.0;
	double distanciabh=0.0;
	double distanciaMI =0.0;
	double aptitudbh = 0;
	int dedosValidados=0;
	File folder = new File(pathTG);
	
	File[] listOfFiles = folder.listFiles();
	File[] listofkeyboards;

	for (File file : listOfFiles) {
		System.out.println("El nombre del folder es: "+file.getName());
		listofkeyboards=file.listFiles();
		for (File fileinfolder : listofkeyboards) {
		System.out.println("El nombre del archivo es: "+fileinfolder.getName());
		
	    if (fileinfolder.isFile()) {
	    	tecladoAleatorio =file.getAbsolutePath()+"\\"+fileinfolder.getName();
	    	//nombreTeclado= fileinfolder.getName();
	    	carpeta= file.getName();
	    MaptecladoAleatorio= CargatecladoAleatorio(tecladoAleatorio);
	   //distancia = EstadisticasGeneralesTA(texto, MaptecladoAleatorio, texto, nombreTeclado, tecMI, tecMD, numManos);
	   if (numManos==1&&mano_utilizar.contains("DERECHA")) {
		  distanciamd = distanciaMD(MaptecladoAleatorio, fingerMD, numManos, dedosDisponibles, posicioninicialx, posicionInicialy, texto);
		  DistanciaMDal.add(distanciamd);
		  posicioninicialx =getPosicioninicialx();
		  posicionInicialy = getPosicionInicialy(); 
		  porcentajeDedosMD = getPorcentajeMD(teclasPulsadasBH, textStatistics, MaptecladoAleatorio);
		  
		 // porcentajeDedosMI= getPorcentajeMI(fingerMD, textStatistics,MaptecladoAleatorio);		   //porcentajeManoMI=  porcenatajeCargaMI(textStatistics, tecMI, MaptecladoAleatorio);
		  porcentajeManoMD= porcenatajeManoMD(texto, textStatistics, tecMI, MaptecladoAleatorio);
		 //  porcentajeDedosMD = getPorcentajeMD(fingerMD, textStatistics, teclado);
dedosValidados = validaFingerPorcentage(porcentajeDedosMD);
nombreTeclado="";	 
aptitudes.add(aptitudmd);
numTeclado++;
nombreTeclado = "teclado_Generado"+numTeclado+".txt";
		aptitudmd =   FA.calculaAptitudMD(DistanciaMDal, dedosValidados, numTeclado, nombreTeclado, porcentajeManoMD);
		   //Ma.EscribeEstadisticasTecladoMD(MaptecladoAleatorio, porcentajeManoMD, aptitudmd, DistanciaMDal, pathEstadisticas, nombreTeclado, posicioninicialx, posicionInicialy, carpeta);
		   Ma.escribeAptitud(pathEstadisticas, carpeta, aptitudmd, nombreTeclado, mano_utilizar, numManos);
	   }else if (numManos==1&&mano_utilizar.contains("IZQUIERDA")) {
		 distanciaMI = distanciaMI(texto, MaptecladoAleatorio, teclasPulsadasLH, numManos, dedosDisponibles);
		 DistanciaMIal.add(distanciaMI);
		 posicioninicialx = getPosicioninicialx();
		 posicionInicialy = getPosicionInicialy();
		 porcentajeDedosMI= getPorcentajeMI(teclasPulsadasLH, textStatistics, MaptecladoAleatorio);
	dedosValidados= validaFingerPorcentage(porcentajeDedosMI);

		 porcentajeManoMI=  porcenatajeManoMI(texto, textStatistics, tecMI, MaptecladoAleatorio);
	//	 if (DistanciaMIal.size()>1) {
aptitudmi = FA.calculaAptitudMI (DistanciaMIal, dedosValidados, numTeclado, nombreTeclado,distanciaMI);
nombreTeclado="";	 
aptitudes.add(aptitudmi);
numTeclado++;
nombreTeclado = "teclado_Generado"+numTeclado+".txt";
//aptitudmi = aptitudst.toString();
Ma.escribeAptitud(pathEstadisticas, carpeta, aptitudmi, nombreTeclado, mano_utilizar, numManos);
//Ma.escribeContenidoSimpliificado(pathEstadisticas, aptitudmi, aptitudmi, carpeta, aptitudst, nombreTeclado);
		// }
//Ma.EscribeEstadisticasTecladoMI(MaptecladoAleatorio, porcentajeManoMI, aptitudmi, DistanciaMIal, pathEstadisticas, nombreTeclado, posicioninicialx, posicionInicialy, carpeta);
		
		  // distanciaAM = EstadisticasGeneralesTA(texto, MaptecladoAleatorio, texto, nombreTeclado, tecMI, fingerStatisticsMI, numManos, dedosDisponibles);
//porcentajeDedosMI=  getPorcentajeMI(teclasPulsadasBH, textStatistics, teclado);
	   }else if (numManos>1) {
		   porcentajeManoMD= porcenatajeManoMD(texto, textStatistics, tecMI, MaptecladoAleatorio);
		   porcentajeManoMI=  porcenatajeManoMI(texto, textStatistics, tecMI, MaptecladoAleatorio);
 		   distanciabh = EstadisticasGeneralesTA(texto, MaptecladoAleatorio, texto, nombreTeclado, tecMI, teclasPulsadasBH, numManos, dedosDisponibles);   
distanciaAM.add(distanciabh);
		   fingerPorcentage= getPorcentaje2M(teclasPulsadasBH, textStatistics, teclado, dedosDisponibles);
	   
	    dedosValidados = validaFingerPorcentage(fingerPorcentage);
	    aptitudbh=  FA.calculaAptitudBH(distanciaAM, dedosValidados, dedosValidados, carpeta, distanciabh);
	  // distanciaAM.add(aptitudbh);
	    nombreTeclado="";	
	    numTeclado++;
	    nombreTeclado = "teclado_Generado"+numTeclado+".txt";
	   // Ma.EscribeEstadisticasTecladoBH(MaptecladoAleatorio, porcentajeManoMD, porcentajeManoMI, aptitudbh, distanciaAM, pathEstadisticas, nombreTeclado, carpeta, posicioninicialx, posicionInicialy);
	 Ma.escribeAptitud(pathEstadisticas, carpeta, aptitudmi, nombreTeclado, mano_utilizar, numManos);
	    
	   }
	    }
	       MaptecladoAleatorio.clear();
	    
	       
	    }
		
	}

}
public void estadisticasQwerty(String textFile, Map<String, Integer> teclado) {
Map<String, Integer>estadisticaTQ= new HashMap<String, Integer>();
	int frecuencia=0;
	StringBuffer sb = new StringBuffer();
	String contenidoFilaarr[];
	String contenidoFilast;
	double distancia;
	
		for (String clave:teclado.keySet()) {
		    int valor = teclado.get(clave);
		    contenidoFilaarr = clave.split(",");
		    for (int i=0; i<contenidoFilaarr.length; i++) {
		    sb.append(contenidoFilaarr[i]);	
		    }
		    contenidoFilast = sb.toString();
		    for (int cf=0; cf<contenidoFilast.length();cf++) {
		    	for (int t=0;t<textFile.length();t++) {
		    if (textFile.charAt(t)== contenidoFilast.charAt(cf)) {
		    	frecuencia++;
		    	estadisticaTQ.put(contenidoFilast.charAt(cf)+" ", frecuencia);
		    	
		    }
	
		    }frecuencia=0;	    distancia = Math.sqrt(frecuencia* 2);
		   // System.out.println("caracteres: " + clave + ", frecuencia: " + valor);
		}
		
	}
	System.out.println("Estadisticas Teclado Qwerty");
	for (String clave:estadisticaTQ.keySet()) {
	    int valor = estadisticaTQ.get(clave);
	    System.out.println("Caracter: " + clave + ", frecuencia: " + valor);
	}
}
public Map<String, Integer> estadistica_texto (String textFile) {
	int frecuencia = 0;
	String Alfabeto = "abcdefghijklmnñopqrstuvwxyz";
	System.out.println("Estadisticas generales del texto");
	Map<String, Integer>textStatistics = new HashMap<String, Integer>();
	
	for (int i=0;i<Alfabeto.length(); i++) {
	for (int j=0;j<textFile.length(); j++) {
		if (Alfabeto.charAt(i)==textFile.charAt(j)) {
			frecuencia++;
			textStatistics.put(Alfabeto.charAt(i)+ " ", frecuencia);
		}
	}
	frecuencia =0;
	}
	for (String clave:textStatistics.keySet()) {
	    int valor = textStatistics.get(clave);
	    System.out.println("caracter: " + clave + ", frecuencia: " + valor);
	}
	return textStatistics;
}

public double porcenatajeManoMI (String texto, Map<String, Integer> textStatistics, Map<String, Integer> tecnicaDigitacionMI, Map<String, Integer>tecladoGenerado) {
	estadisticaMI = putEstadisticasMI(texto, tecnicaDigitacionMI, tecladoGenerado);
	double porcentaje = 0;
	String contenido;
	double porcentajeDedo = 0;
	int total =100;
	String[] contenidoFila;

	StringBuilder sb = new StringBuilder();
	for (String clave:tecladoGenerado.keySet()) {
	    int valor = tecladoGenerado.get(clave);
	   // System.out.println("Clave: " + clave + ", valor: " + valor);
	    contenidoFila = clave.split(",");
	   
	    for (String clavets:textStatistics.keySet()) {
		    int valorts = textStatistics.get(clavets);
		    
		    for (String clavetpmi:estadisticaMI.keySet()) {
			    int valortpmi = estadisticaMI.get(clavetpmi);
			   // System.out.println("caracter: " + clavetd + ", Frecuencia: " + valortd);
			    contenido = clavetpmi;
			    for (int i=0;i<contenidoFila.length;i++) {
			    if (contenidoFila[i].equals(contenido)) {
			    //	System.out.println("Entre al if de carga trabajo mano");
			    	porcentaje = valortpmi *total;
			    	porcentaje = porcentaje/valortpmi;
			    	porcentajeDedo+= porcentaje;
			    	porcentajeDedo= porcentajeDedo/valorts;
			    	
			    	
			    }
			    
			    }
			    
		    }
		    
	 }
	}
	System.out.println("  la mano izquierda tiene una carga de trabajo de : "+porcentajeDedo + " %");
	return porcentajeDedo;
    
}






public double porcenatajeManoMD (String texto, Map<String, Integer> textStatistics, Map<String, Integer> tecnicaDigitacionMI, Map<String, Integer>tecladoGenerado) {
	estadisticaMI = putEstadisticasMI(texto, tecnicaDigitacionMI, tecladoGenerado);
	double porcentaje = 0;
	String contenido;
	double porcentajeDedo = 0;
	int total =100;
	String[] contenidoFila;

	StringBuilder sb = new StringBuilder();
	for (String clave:tecladoGenerado.keySet()) {
	    int valor = tecladoGenerado.get(clave);
	   // System.out.println("Clave: " + clave + ", valor: " + valor);
	    contenidoFila = clave.split(",");
	   
	    for (String clavets:textStatistics.keySet()) {
		    int valorts = textStatistics.get(clavets);
		    
		    for (String clavetpmi:estadisticaMI.keySet()) {
			    int valortpmi = estadisticaMI.get(clavetpmi);
			   // System.out.println("caracter: " + clavetd + ", Frecuencia: " + valortd);
			    contenido = clavetpmi;
			    for (int i=0;i<contenidoFila.length;i++) {
			    if (contenidoFila[i].equals(contenido)) {
			    	//System.out.println("Entre al if de carga trabajo mano");
			    	porcentaje = valortpmi *total;
			    	porcentaje = porcentaje/valortpmi;
			    	porcentajeDedo+= porcentaje;
			    	porcentajeDedo= porcentajeDedo/valorts;
			    	
			    	
			    }
			    
			    }
			    
		    }
		    
	 }
	}
	System.out.println("La carga de trabajo de la mano izquierda es : "+porcentajeDedo + " %");
	return porcentajeDedo;
    
}
public Map<String, Integer> CargatecladoAleatorio(String pathTA) {
	int numFila=0;
	ArrayList<Integer>Individuo = new ArrayList<Integer> ();
	String []filasTeclado;

	try {
		List<String> allLines = Files.readAllLines(Paths.get(pathTA));

		for (String line : allLines) {
			numFila++;
			
			//System.out.println(line);
			filasTeclado = line.split("[\\(||\\)]");
			System.out.println(line);
			
		
			//Teclado.add(line);
			MaptecladoAleatorio.put(line, numFila);
		}
	} catch (IOException e) {
		e.printStackTrace();
	}
	return MaptecladoAleatorio;
	
}
public void porcentajeDedosMI () {
	for (String clave:estadisticaMI.keySet()) {
	    int valor = estadisticaMI.get(clave);
	    System.out.println("Clave: " + clave + ", valor: " + valor);
	}
}
public void pulsacionesMI(Map<String, Integer> tecnicaDigitacionMD, Map<String, Integer> tecnicaDigitacionMI, String texto) {
	//System.out.println("Imprimiendo estadisticas de la mano derecha");
	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	
	//int frecuencia = 0;
	for (String clave:tecnicaDigitacionMI.keySet()) {
	    int valor = tecnicaDigitacionMI.get(clave);
	    System.out.println("contenidoFila: " + clave + ", numDedo: " + valor);
	    contenidoFila = clave.split(",");
	    for (int i =0;i<contenidoFila.length;i++) {
	    	
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<texto.length(); t++) {
	    		if (texto.charAt(t)== contenidoFila[i].charAt(cc)) {
	    		fingerStatisticsMI.put(contenidoFila[i]+"", valor);
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMI.put( contenidoFila[i],frecuenciaPulsacion);
	    			
	    			
	    		}
	    		
	    	}
	    	}
	    	
	    	
	    	
	    }
	}	
	System.out.println("\n Imprimiendo estadisticas por dedo de la mano derecha");
	for (String clave:fingerStatisticsMI.keySet()) {
	    int valor = fingerStatisticsMI.get(clave);
	    System.out.println("caracter: " + clave + ", frecuencia: " + valor);
	}
	
}
public Map<String, Integer> put_pulsacionesDedosMI (Map<String, Integer>tecnicaDigitacionMI, String texto, Map<String, Integer> teclados) {

	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	
	int frecuencia = 0;
	for (String clavetmi:tecnicaDigitacionMI.keySet()) {
	    int valor = tecnicaDigitacionMI.get(clavetmi);
	    System.out.println("contenidoFila: " + clavetmi + ", numDedo: " + valor);
	    contenidoFila = clavetmi.split(",");
	    for (int i =0;i<contenidoFila.length;i++) {
	    	
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<texto.length(); t++) {
	    		if (texto.charAt(t)== contenidoFila[i].charAt(cc)) {
	    		fingerStatisticsMI.put(contenidoFila[i]+"", valor);
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMI.put( contenidoFila[i],frecuenciaPulsacion);
	    			
	    			
	    		}
	    		
	    	}
	    	}
	    	
	    	
	    	
	    }
	}
	System.out.println("Impromiendo estadísticas por dedo de la mano Izquierda");
	for (String clavef:fingerStatisticsMI.keySet()) {
	    int valorf = fingerStatisticsMI.get(clavef);
	    System.out.println("caracter: " + clavef + ", numDedo: " + valorf);
	}
	return fingerStatisticsMI;		
}
public Map<String, Integer> putEstadisticasMD( Map<String, Integer> tecnicaDigitacionMD, String texto,Map<String, Integer> teclado) {
	this.textFile = texto;

	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	String teclaPulsada="";
	

	int freecuencia = 0;
	for (String clave:teclado.keySet()) {
		for (String clavetd:tecnicaDigitacionMD.keySet()) {
			int valortd =tecnicaDigitacionMD.get(clavetd);
		
	    int valor = teclado.get(clave);
	   // System.out.println("contenidoFila: " + clave + ", Dedo encargado: " + valor);
	    contenidoFila = clave.split(",");
	    for (int i =1;i<contenidoFila.length-1;i++) {
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<textFile.length(); t++) {
	    		if (textFile.charAt(t)== contenidoFila[i].charAt(cc)) {
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMD.put( contenidoFila[i],frecuenciaPulsacion);
	    		}	
	    			
	    		}
	    		
	    	}
	    		frecuenciaPulsacion=0;
	    	}
	    	
	    	
	    	
	    }
	    
	}
	System.out.println("Imprimiendo estadisticas de la mano derecha");
	for (String clave:estadisticaMD.keySet()) {
	    int valor = estadisticaMD.get(clave);
	    System.out.println("caracter: " + clave + ", frecuencia: " + valor);
	}
	return estadisticaMD;
	
}
public Map<String, Integer> put_pulsacionesDedosMD (Map<String, Integer>tecnicaDigitacionMD, String texto, Map<String, Integer> teclados) {

	String contenidoFila [];
	String contenidoColumnas;
	int frecuenciaPulsacion=0;
	
	int frecuencia = 0;
	for (String clavetmi:tecnicaDigitacionMD.keySet()) {
	    int valor = tecnicaDigitacionMD.get(clavetmi);
	    System.out.println("contenidoFila: " + clavetmi + ", numDedo: " + valor);
	    contenidoFila = clavetmi.split(",");
	    for (int i =0;i<contenidoFila.length;i++) {
	    	
	    	//System.out.println(contenidoFila[i]);
	    	contenidoColumnas = contenidoFila[i];
	    	for (int cc=0;cc<contenidoColumnas.length();cc++) {
	    		for (int t=0;t<texto.length(); t++) {
	    		if (texto.charAt(t)== contenidoFila[i].charAt(cc)) {
	    		fingerStatisticsMD.put(contenidoFila[i]+"", valor);
	    			frecuenciaPulsacion++;
	    		//System.out.println("Entre Crgas de Trabajo num Dedo " + tecnicaDigitacionMD.get(clave));
	    			estadisticaMI.put( contenidoFila[i],frecuenciaPulsacion);
	    			
	    			
	    		}
	    		
	    	}
	    	}
	    	
	    	
	    	
	    }
	}
	System.out.println("Impromiendo estadísticas por dedo de la mano Derecha");
	for (String clavef:fingerStatisticsMD.keySet()) {
	    int valorf = fingerStatisticsMD.get(clavef);
	    System.out.println("caracter: " + clavef + ", numDedo: " + valorf);
	}
	return fingerStatisticsMD;

}


public void porcentajeCargaDedosMI (Map<String, Integer>fingerstatistics, Map<String, Integer>estadisticasDedos, Map<String, Integer>estadisticastexto, Map<String, Integer>teclado) {
double aux=0.0;
	this.textStatistics =estadisticastexto;
	
	this.textStatistics =estadisticastexto;
	for (String clave:estadisticastexto.keySet()) {
	    int valor = estadisticastexto.get(clave);
	   // System.out.println("Clave: " + clave + ", valor: " + valor);
	    for (String clavefs:fingerStatisticsMD.keySet()) {
	    	
	        int valorfs = fingerStatisticsMD.get(clave);
	    
	        
	        System.out.println("NumDedo: " + clavefs + ", porcentaje: " + valorfs);
	        
	    }    
}

}

public int validaFingerPorcentage (Map<Integer, Double> fingerPorcentage) {
	Double aux=0.0;
	int dedosVlidados=0;

	for (Integer clave:fingerPorcentage.keySet()) {
		Double valoract = fingerPorcentage.get(clave);
		Double valorfut = fingerPorcentage.get(clave+1);
		if (valorfut!=null) {
			
		
			
			
			
			 if (valoract<=valorfut) {
				 
				 System.out.println("La carga de trabajo del dedo "+ clave + " cumple con el criterio");
			        dedosVlidados+=1;
			        if (dedosVlidados>=5) {
			        	boolean validacion = true;
			        	 System.out.println("SE CUMPLIÓ LA VALIDACIÓN DE HABILIDAD POR DEDO ");
			        }
			        }
		}
	   
	    
	   // System.out.println("NumDedo: " + clave + ", porcentaje: " + valoract);
	 
       
	}
//MaptecladoAleatorio.clear();
if (dedosVlidados==0){
	dedosVlidados=1;
}
return dedosVlidados;
}
public TreeMap<Integer, Double> getPorcentaje2M (Map<String, Integer>teclasPulsadasMD, Map<String, Integer>textStatistics, Map<String, Integer>teclado, String dedosDisponibles) {
	String contenidoFila[];
	double porcentaje=0.0;
	int total =50;

	String contenido;
	double porcentajeDedo = 0;
	for (String clavet:teclado.keySet()) {
		contenidoFila= clavet.split(",");
		contenidoFila = clavet.split(",");
		for (int i=0;i<contenidoFila.length; i++) {

		//System.out.println("El caracter de la fila es: " +contenidoFila[cf]);
	
		for (String clavets:textStatistics.keySet()) {
			
			int valorts = textStatistics.get(clavets);
			for (String clavetpmd:teclasPulsadasMD.keySet()) {
				Integer valortpmd = teclasPulsadasMD.get(clavetpmd);
				contenido = clavetpmd;
			if (contenidoFila[i].contains(contenido)) {
				System.out.println("Entre al if de porcentaje  en el caracter:"+clavetpmd);
				porcentaje = valorts* total;
				porcentaje = porcentaje / valorts;	    
		    	porcentajeDedo+= porcentaje;
		    	porcentajeMD.put(valortpmd, porcentajeDedo);
			}
			
		}
	}
	}
	}porcentajeDedo=0;

	for (int clavepmd:porcentajeMD.keySet()) {
		double valorpmd = porcentajeMD.get(clavepmd);
    
        
        System.out.println("NumDedo: " + clavepmd + ", porcentaje: " + valorpmd+ " %");
	}
	
	
	return porcentajeMD;

	
}

public void CargaIndividuos(String pathPoblacion, File file) {
	int numFila=0;
	ArrayList<Integer>Individuo = new ArrayList<Integer> ();
	String []filasTeclado;

	try {
		List<String> allLines = Files.readAllLines(Paths.get(pathPoblacion));

		for (String line : allLines) {
			numFila++;
			
			//System.out.println(line);
			filasTeclado = line.split(",");
			
			System.out.println(line);
			
		
			//Teclado.add(line);
			MaptecladoAleatorio.put(line, numFila);
		}
	} catch (IOException e) {
		e.printStackTrace();
	}

}
public double distanciaMD(Map<String, Integer>teclado, Map<String, Integer>teclasPulsadasMD, int numManos, String dedosDisponobles, int posX, int posY, String texto) {
	ArrayList<Double>DistanciaMDal = new ArrayList<Double>();
	
	

	
	int numColumnas= 10;
	int numFilas=4;
	int PI = 0;
	int x1;
	int x2;
	int y1;
	int y2;
	String caracter="";
	PI=posicioninicialx- posicionInicialy;
	posicioninicialx= (int)(Math.random()*numColumnas- dedosDisponobles.length());
	posicionInicialy= (int)(Math.random()*numFilas);
	
	while(posicioninicialx<1) {
		posicioninicialx = posicioninicialx+dedosDisponobles.length();
		if (posicioninicialx<1) {
			posicioninicialx = posicioninicialx * posicioninicialx;
		}
		
	}
	System.out.println("La posición inicial de la mano fué en la columna:  "+ posicioninicialx);
	System.out.println("\n La posición inicial de la mano fué en la fila:  "+ posicionInicialy);
	if (PI<1) {
		PI = PI*PI;
	}else if (numManos>1) {
		PI=0;
		
	}

	int numFila = 0;
	String filaTeclado[];

	double distanciaMD=0.0;
	for (String clavet:teclado.keySet()) {
		numFila++;
		for (String clavetpmd: teclasPulsadasMD.keySet()) {
			int valortpmd = teclasPulsadasMD.get(clavetpmd);
			filaTeclado = clavet.split(",");
			for (int i=PI;i<filaTeclado.length; i++) {
			for (int cf=0;cf<filaTeclado[i].length();cf++) {
				
				for (int tx=0; tx <texto.length(); tx++) {
					caracter = texto.charAt(tx)+"";
				if (filaTeclado[i].contains(clavetpmd)&&filaTeclado[i].contains(caracter)) {
					x1=i;
					x2=valortpmd;
					y1 = cf;
					y2=numFila;
					distanciaMD = x2-x1;
					distanciaMD= y2 - y1;
					
					distanciaMD = Math.pow(x1, x2);
					distanciaMD= Math.pow(y2, y1);
					distanciaMD =distanciaMD+distanciaMD;
					distanciaMD = Math.sqrt(distanciaMD);
				}
					
					
				}
			}
		
			
				
			}
		
			
		
		
		
			
		}
		
		
	
	}
	System.out.println("La distancia de la mano derecha fué: "+distanciaMD);
	DistanciaMDal.add(distanciaMD);
	
	return distanciaMD;
	
	
}
public int getPosicioninicialx() {
	return posicioninicialx;
}

public void setPosicioninicialx(int posicioninicialx) {
	this.posicioninicialx = posicioninicialx;
}

public int getPosicionInicialy() {
	return posicionInicialy;
}

public void setPosicionInicialy(int posicionInicialy) {
	this.posicionInicialy = posicionInicialy;
}

public int getPosicionx() {
	return posicioninicialx;
}

public void setPosicionx(int posicionionicialx, int posicioninicialx) {
	this.posicioninicialx = posicioninicialx;
}

public int getPosiciony() {
	return posicionInicialy;
}

public void setPosiciony(int posiciony) {
	this.posicionInicialy = posiciony;
}

public double distanciaMI(String texto, Map<String, Integer>teclado, Map<String, Integer>teclasPulsadasMI, int numManos, String dedosdDisponibles) {
	
	ArrayList<Double>DistanciaMIal= new ArrayList<Double>(); 
	
	int validaPosicion;
	int posicionInicialy;
	int numColumnas= 10;
	int numFilas=4;
	int PI = 0;	
	int x1;
	int x2;
	int y1;
	int y2;
	
	posicioninicialx= (int)(Math.random()*numColumnas);
	if (posicioninicialx+ dedosdDisponibles.length()>numColumnas) {
		posicioninicialx= (int)(Math.random()*numColumnas- dedosdDisponibles.length());
		while(posicioninicialx<1) {
			posicioninicialx = posicioninicialx+dedosdDisponibles.length();
			if (posicioninicialx<1) {
				posicioninicialx = posicioninicialx * posicioninicialx;
			}
		}
			
		
	}
	posicionInicialy= (int)(Math.random()*numFilas);
	System.out.println("La posición inicial de la mano fué en la columna:  "+ posicioninicialx);
	System.out.println("\n La posición inicial de la mano fué en la fila:  "+ posicionInicialy);
	PI=posicioninicialx- posicionInicialy;
	if (PI<1) {
		PI = PI*PI;
	}else if (numManos>1) {
		PI=0;
		
	}
	String letra;
	int numFila = 0;
	String filaTeclado[];

	double distanciaMI=1.0;
	for (String clavet:teclado.keySet()) {
		numFila++;
		for (String clavetpmi: teclasPulsadasMI.keySet()) {
			int valortpmi = teclasPulsadasMI.get(clavetpmi);
			filaTeclado = clavet.split(",");
			for (int i=PI;i<filaTeclado.length; i++) {
			for (int cf=0;cf<filaTeclado[i].length();cf++) {
				letra = filaTeclado[i].charAt(cf)+"";
				
				if (filaTeclado[cf].contains(clavetpmi)) {
					x1=i;
					x2=cf;
					y1 = valortpmi;
					y2=numFila;
					distanciaMI = x2-x1;
					distanciaMI= y2 - y1;
					
					distanciaMI = Math.pow(x1, x2);
					distanciaMI= Math.pow(y2, y1);
					distanciaMI =distanciaMI+distanciaMI;
					distanciaMI = Math.sqrt(distanciaMI);
					
					
				}
			}

			
				
			}
	
			
		
		
		
			
		}
		
		
	
	}
	System.out.println("La distancia de la mano izquierda fué: "+distanciaMI);
	DistanciaMIal.add(distanciaMI);
	return distanciaMI;
	
	
}
TreeMap<Integer, Double> porcentajeMD = new TreeMap<Integer, Double>();
public TreeMap<Integer, Double> getPorcentajeMD (Map<String, Integer>teclasPulsadasMD, Map<String, Integer>textStatistics, Map<String, Integer>tecladoMap) {
	String contenidoFila[];
	double porcentaje;
	double porcentajeDedo = 0;
	int total =50;

	String contenido;

	for (String clavet:tecladoMap.keySet()) {
		contenidoFila= clavet.split(",");
		contenidoFila = clavet.split(",");
		for (int i=0;i<contenidoFila.length; i++) {

		//System.out.println("El caracter de la fila es: " +contenidoFila[cf]);
	
		for (String clavets:textStatistics.keySet()) {
			
			int valorts = textStatistics.get(clavets);
			for (String clavetpmd:teclasPulsadasMD.keySet()) {
				Integer valortpmd = teclasPulsadasMD.get(clavetpmd);
				contenido = clavetpmd;
			if (contenidoFila[i].contains(contenido.trim())) {
				//System.out.println("Entre al if de porcentaje  en el caracter:"+contenido);
				porcentaje = valorts* total;
				porcentaje = valorts / porcentaje;
				porcentaje = porcentaje/ valorts;
		    
		    	porcentajeDedo+= porcentaje;
		    	porcentajeDedo= porcentajeDedo;
		    	porcentajeDedosMD.put(valortpmd, porcentajeDedo);
			}
			
		}
	}
	}
	}porcentajeDedo=0;

	for (int clavepmd:porcentajeDedosMD.keySet()) {
		double valorpmd = porcentajeDedosMD.get(clavepmd);
    
        
        System.out.println("NumDedo: " + clavepmd + ", porcentaje: " + valorpmd+ " %");
	}
	
	
	return porcentajeDedosMD;
	
	
}
public TreeMap<Integer, Double> getPorcentajeMI (Map<String, Integer>teclasPulsadasMD, Map<String, Integer>textStatistics, Map<String, Integer>teclado) {
	String contenidoFila[];
	double porcentaje;
	double porcentajeDedo = 0;
	int total =50;

	String contenido;

	for (String clavet:teclado.keySet()) {
		contenidoFila= clavet.split(",");
		contenidoFila = clavet.split(",");
		for (int i=0;i<contenidoFila.length; i++) {

		//System.out.println("El caracter de la fila es: " +contenidoFila[cf]);
	
		for (String clavets:textStatistics.keySet()) {
			
			int valorts = textStatistics.get(clavets);
			for (String clavetpmd:teclasPulsadasMD.keySet()) {
				Integer valortpmd = teclasPulsadasMD.get(clavetpmd);
				contenido = clavetpmd;
			if (contenidoFila[i].contains(contenido.trim())) {
			//	System.out.println("Entre al if de porcentaje  en el caracter:"+contenido);
				porcentaje = valorts* total;
				porcentaje = valorts / porcentaje;
				porcentaje = porcentaje/ valorts;
		    
		    	porcentajeDedo+= porcentaje;
		    	porcentajeDedo= porcentajeDedo;
		    	porcentajeDedosMI.put(valortpmd, porcentajeDedo);
			}
			
		}
	}
	}
	}porcentajeDedo=0;

	for (int clavepmd:porcentajeDedosMI.keySet()) {
		double valorpmd = porcentajeDedosMI.get(clavepmd);
    
        
        System.out.println("NumDedo: " + clavepmd + ", porcentaje: " + valorpmd+ " %");
	}
	
	
	return porcentajeDedosMI;
	
}
public Map<Integer, Double> getPorcentajeBH (Map<String, Integer>teclasPulsadasMD, Map<String, Integer>textStatistics, Map<String, Integer>teclado) {
	String contenidoFila[];
	double porcentaje;
	int total =50;

	String contenido;
	double porcentajeDedo = 0;
	for (String clavet:teclado.keySet()) {
		contenidoFila= clavet.split(",");
		contenidoFila = clavet.split(",");
		for (int i=0;i<contenidoFila.length; i++) {

		//System.out.println("El caracter de la fila es: " +contenidoFila[cf]);
	
		for (String clavets:textStatistics.keySet()) {
			
			int valorts = textStatistics.get(clavets);
			for (String clavetpmd:teclasPulsadasMD.keySet()) {
				Integer valortpmd = teclasPulsadasMD.get(clavetpmd);
				contenido = clavetpmd;
			if (contenidoFila[i].contains(contenido.trim())) {
				System.out.println("Entre al if de porcentaje  en el caracter:"+contenido);
				porcentaje = valorts* total;
				porcentaje = valorts / porcentaje;
				porcentaje = porcentaje/ valorts;
		    
		    	porcentajeDedo+= porcentaje;
		    	porcentajeDedo= porcentajeDedo;
		    	porcentajeMD.put(valortpmd, porcentajeDedo);
			}
			
		}
	}
	}
	}porcentajeDedo=0;

	for (int clavepmd:porcentajeMD.keySet()) {
		double valorpmd = porcentajeMD.get(clavepmd);
    
        
        System.out.println("NumDedo: " + clavepmd + ", porcentaje: " + valorpmd+ " %");
	}
	
	
	return porcentajeMD;

	
}
public double porcenatajeCargaMDe (Map<String, Integer> textStatistics, Map<String, Integer> tecnicaDigitacionMD, Map<String, Integer>tecladoGenerado) {
	double porcentaje = 0;
	String contenido;
	double porcentajeDedo = 0;
	int total =50;
	String[] contenidoFila;


	for (String clave:tecladoGenerado.keySet()) {
	    int valor = tecladoGenerado.get(clave);
	   // System.out.println("Clave: " + clave + ", valor: " + valor);
	    contenidoFila = clave.split(",");
	 

	    for (String clavetets:textStatistics.keySet()) {
		    int valortext = textStatistics.get(clavetets);
		    
		    for (String clavetd:estadisticaMD.keySet()) {
			    int valortd = estadisticaMD.get(clavetd);
			   // System.out.println("caracter: " + clavetd + ", Frecuencia: " + valortd);
			    contenido = clavetd;
			    for (int i=0;i<contenidoFila.length;i++) {
			    if (contenidoFila[i].contains(contenido.trim())) {
			    	porcentaje = valortd *total;
			    	porcentaje = porcentaje/total;
			    	porcentajeDedo+= porcentaje;
			    	
			    }
			    
			    }
			    
		    }
		    
	 }
	}
	System.out.println("La carga de trabajo de la mano derecha es : "+porcentajeDedo + " %");
	return porcentajeDedo;
    
}

public ArrayList<Integer>SeleccionTorneo(String pathTG, String folderg, int numIndividuos, String patpoblacion, int sizeTorneo, int numgeneracion) throws IOException {
	numIndividuos = numIndividuos-2;

	int competidorRandom;
	int numCompetidores=sizeTorneo;
	
	
	
	ArrayList<Integer> competidores =new ArrayList<Integer>();
	for (int i=0;i<sizeTorneo;i++) {
		competidorRandom = (int)(Math.random()*numIndividuos+1);;
		competidores.add(competidorRandom);
		
		
	}

	
	String tecladoAleatorio="";
	String []FileContent;
	double competidor=0.0;
	String palabra="";
	double aptitud = 0 ;
	int numCompetidor = 0;
	int numGanadores=0;
	int padreAusente=0;
	String tecladoCompetir="";
	double ganador = 0;
	
	ArrayList<Double>aptitudes = new ArrayList<Double>();
	
	ArrayList<Double>aptitudTeclados= new ArrayList<Double>();
	ArrayList<Integer> padres= new ArrayList<Integer>();
File folder = new File(pathTG+ "\\"+ folderg);
	String content="";
	File[] listOfFiles = folder.listFiles();
	File[] listofkeyboards;
	for (File file : listOfFiles) {
	if (listOfFiles.length==0) {
		numgeneracion-=1;
		//folderg = folderg +numgeneracion;
	}
		
	
	//	System.out.println("El nombre del folder es: "+folder.getName());
		listofkeyboards=folder.listFiles();
		for (File fileinfolder : listofkeyboards) {
			if(numCompetidores>listofkeyboards.length-1) {
				numCompetidores = numCompetidores-2;
				if (fileinfolder.isFile()) {
					//numArchivo++;
					tecladoAleatorio =fileinfolder.getAbsolutePath();
			    	nombreTeclado= fileinfolder.getName();
			    	content = new String(Files.readAllBytes(Paths.get(tecladoAleatorio)));
			    	aptitud = Double.valueOf(content);
			    	aptitudes.add(aptitud);
			    
			    	//Se termina de cargar las aptitudes de los teclados 
			    	
				}
				
			}
			
			
		
		
			
		}
//		competencia entre aptitudes 
		for (int i=1;i<competidores.size()-1; i++) {
			numTeclado++;
     		numCompetidor =competidores.get(i);
 
    		tecladoCompetir= pathTG+"\\"+ folderg + "\\"+listofkeyboards[numCompetidor].getName();
    		
    				content = new String(Files.readAllBytes(Paths.get(tecladoCompetir)));
    				//ganador = aptitudes.get(0);
    				aptitud = Double.valueOf(content);
    				aptitudTeclados.add(aptitud);
		}
		//Se termina de cargar todas las aptitudes de los teclados 
		
		//Comienza el torneo entre aptitudes de los teclados/individuos 
		aptitud = aptitudTeclados.get(0);
		for (int ap=1; ap<aptitudTeclados.size()-1; ap++) {
			//aptitud = aptitudTeclados.get(ap);
			competidor = aptitudTeclados.get(ap);
			if (competidor<aptitud) {
				ganador= aptitud;
				System.out.println("el valor del ganador es : "+ganador);
				if (!padres.contains(numCompetidor)) {
				padres.add(numCompetidor);
				}
				//System.out.println("el valor de la aptitud  es : "+aptitud);
				numGanadores++;
				if (padres.size()!=2) {
					for (int p=0;p<padres.size(); p++) {
					if (!padres.contains(padres.get(p))) {
						padres.add(padres.get(p));
					
					}
					}
					
					
				}
				
				
				
				
			//	return padres;
				
			
	    		
	    	}
		}
    					
    					
    				
    		    	
    		    	
    		    	
    			
    					
    	 
    	 		
    	 		
    		
    		


    		
    		
    	
	numTeclado=0;
	aptitudes.clear();
	aptitudTeclados.clear();
}

	

	





	
	File[] file = folder.listFiles();
	System.out.println("El nombre del folder es: ");
	if (padres.size()==1) {
		int padrefaltante = (int)(Math.random()*numIndividuos+1);
		padres.add(padrefaltante);
	}
	return padres;







}
public void cruzaEmparejamiento (ArrayList<Integer>Padres, String pathPoblacion, int numGeneracion, String folderg, String pathst, int numGenes, int numindividuos) throws IOException {
numGenes = numGenes-3;
ArrayList<Integer> indiceArchivo =new ArrayList<Integer>();	
	cruza=false;

this.pathpoblacionglobal = pathPoblacion+ "\\"+ folderg;
//String nombreindHijo="";
Ma.creaFolder(pathpoblacionglobal);

 numGeneracion = numGeneracion -1;
	ArrayList<Integer> Hijo1 = new ArrayList<Integer>();
	ArrayList<Integer> Hijos = new ArrayList<Integer>();
	ArrayList<Integer> Hijo2 = new ArrayList<Integer>();
	ArrayList<Integer> Padre1 = new ArrayList<Integer>();
	ArrayList<Integer> Padre2 = new ArrayList<Integer>();
	String content="";

	String pathaptitudes = pathst+ "\\"+folderg;
	String []contenidoIndividuo=null;
	int numPadre=0;
	int contadorh1=0;
	int contadorh2=0;
	int numrepetidosh1=0;
	int numrepetidosh2=0;
	int indice=0;
	int contenidoPadres=0;
String getPadres="";
int permutacion=0;
//int numHijo=0;
int permutacion2=0;
String getIndividuos="";
String caracter="";

	File folder = new File(pathPoblacion);
	//File folderactpop = new File(pathP);
	File folderapt = new File(pathaptitudes);

	File[] listOfFiles = folder.listFiles();
	File[] listofkeyboards = folderapt.listFiles();
	for (File file : listOfFiles) {
		getIndividuos= file.getName();
		System.out.println("El nombre del folder es: "+file.getName());
		
		listofkeyboards=file.listFiles();
		for (File fileinfolder : listofkeyboards) {
			if ( cruza==true) {
				return;
			}
			System.out.println("El nombre del archivo es: "+fileinfolder.getName());
			if (fileinfolder.isFile()) {
			
			for(int i=0;i<Padres.size();i++) {
				numPadre++;
				getPadres =file.getAbsolutePath()+"\\"+fileinfolder.getName();
				getIndividuos=file.getAbsolutePath()+"\\"+listofkeyboards[Padres.get(i)+1].getName();
				content = new String(Files.readAllBytes(Paths.get(getIndividuos)));
				contenidoIndividuo = content.split(",");
				System.out.println("El padre es :"+getIndividuos+" con una aptitud de: "+content);
				for (int pa=0;pa<contenidoIndividuo.length;pa++) {
					if (numPadre==1) {
					caracter = contenidoIndividuo[pa];
					caracter = caracter.trim();
	caracter = caracter.replaceAll("\\[", "").replaceAll("\\]","");
						permutacion = Integer.parseInt(caracter);
						Padre1.add(permutacion);
						
							
						}else if (numPadre==2) {
							caracter = contenidoIndividuo[pa];
							caracter = caracter.trim();
			caracter = caracter.replaceAll("\\[", "").replaceAll("\\]","");
								permutacion = Integer.parseInt(caracter);
								Padre2.add(permutacion);	
						}else if (numPadre>=3) {
							break;
						}
							
							
								
								
							}
						
						
						
					}
			int pCorte1 = (int)(Math.random()*numGenes+1);
			int pCorte2 = (int)(Math.random()*numGenes+ pCorte1);
			if (pCorte1==pCorte2) {
				pCorte2 = pCorte1+pCorte2;
				pCorte1 = pCorte1+5;
			}else if (pCorte2>numGenes) {
				pCorte2 = pCorte2-1;
				pCorte1 = pCorte2-pCorte1; 
				pCorte2 = numGenes;
				if (pCorte1>pCorte2) {
			pCorte1 = pCorte1 -pCorte2;
				}
			}
			
			// Rellenar hijo 2 de acuerdo a puntos de corte
			contenidoPadres = pCorte2 - pCorte1;
			for (int hi2=pCorte1; hi2<pCorte2; hi2++) {
				
				Hijo2.add(Padre1.get(hi2));
			
			}
			// Rellenar hijo 1 de acuerdo a puntos de corte
						contenidoPadres = pCorte2 - pCorte1;
						
						for (int hi1=pCorte1; hi1<pCorte2; hi1++) {
							//Hijo1.add(Padre2.get(hi1));
							//contador++;
						if (!Hijo2.contains(Padre1.get(hi1))){
							Hijo1.add(Padre1.get(hi1));
						}
						}
						// Rellenar los elementos faltantes en el hijo 2
				
Hijo2.addAll(Hijo1);
while (Hijo2.size()<contenidoIndividuo.length-1) {

for (int i =0;i<contenidoIndividuo.length-1; i++) {
	permutacion = Padre1.get(i);
	if (!Hijo1.contains(permutacion)){
		if (!Hijo2.contains(permutacion)){
			Hijo2.add(Padre1.get(i));	
		
		}else {
			//Hijo2.add(Padre2.get(i));	
		}
	}
}
//numHijo+=1;


		//contadorh1 = contadorh1+ contadorh2;
		
	
}
Hijo2g = Hijo2;
contadorh1=0;
numrepetidosh2=0;



while (Hijo1.size()<contenidoIndividuo.length) {

	for (int i =0;i<contenidoIndividuo.length; i++) {
		permutacion = Padre2.get(i);
		if (!Hijo1.contains(permutacion)){
			if (!Hijo2.contains(permutacion)){
				Hijo2.add(Padre2.get(i));	
			
			}else {
				Hijo1.add(Padre1.get(i));	
			}
		}
	}



		//contadorh1 = contadorh1+ contadorh2;
		
	
}
Hijo1g = Hijo1;
cruza = true;



				
				
				
			
			
				
		
				
				
				
			
				
			}
			//numPadre=0;
			numHijo+=1;
			nombreindHijo= "Hijo_"+numHijo;
			Ma.EscribeHijos(pathPoblacion, Hijo1, nombreindHijo, folderg);
			int fileIndex = (int)(Math.random()*numindividuos+1);
			indiceArchivo.add(fileIndex);
			if (numGeneracion==0) {
			Ma.borraArchivo(pathPoblacion, folderg, indiceArchivo, numGeneracion);
			}
			//Hijo1.clear();
			numHijo++;
			nombreindHijo= "Hijo_"+numHijo;
			Ma.EscribeHijos(pathPoblacion, Hijo2, nombreindHijo, folderg);
			int fileId = (int)(Math.random()*numindividuos+1);
			indiceArchivo.add(fileId);
			if (numGeneracion==0) {
			Ma.borraArchivo(pathPoblacion, folderg, indiceArchivo, numGeneracion);
			}
			//mutacionHijos(Hijo1, Hijo2);
			//Hijo2.clear();
		//return;
			
		}
	}

}

public void mutacionHijos(ArrayList<Integer>Hijo1, ArrayList<Integer>Hijo2) throws IOException {
int numHijo=0;
	
	double probMutacion;
	int getgen2;
	probMutacion= 1/probabilidadMutacion;
	double naMutacion = Math.random();
	if (naMutacion>probMutacion) {
		System.out.println("se aplicará mutación");
		for (int i=0;i<Hijo1.size(); i++) {
			double mutagen =Math.random();
		if (mutagen>probMutacion) {
 getgen2 = (int)(Math.random()*Hijo1.size()+1);
Hijo1.add(i,getgen2);
			
		}
		
		}
		

		for (int i=0;i<Hijo1.size(); i++) {
			double mutagen =Math.random();
		if (mutagen>probMutacion) {
 getgen2 = (int)(Math.random()*Hijo1.size()+1);
Hijo1.add(i,getgen2);
			
		}
		
			
		}
		numHijo+=1;
		nombreindHijo= "Hijo_"+numHijo;
		Ma.EscribeHijos(pathpoblacionglobal, Hijo1, nombreindHijo, Folder);	
		
		for (int i=0;i<Hijo2.size(); i++) {
			double mutagen =Math.random();
		if (mutagen>probMutacion) {
 getgen2 = (int)(Math.random()*Hijo2.size()+1);
Hijo1.add(i,getgen2);
			
		}
	
		}
		numHijo+=1;
		nombreindHijo= "Hijo_"+numHijo;
		Ma.EscribeHijos(pathpoblacionglobal, Hijo2, nombreindHijo, Folder);	
		cruza=true;
		//return;
	}
	return;

}
public ArrayList<Integer> SeleccionTorneov2 (String PathPop, String patST, String folderg, int lenghtTorneo, int numIndividuos) throws IOException {

	ArrayList<Double> aptitudes = new ArrayList<Double>();

	String contenidoArchivo="";
	String tecladoCompetir;
	double aptitudtorneo;
	String content ="";
	double aptitud;
	double aux=0.0;
	double competidor =0.0;
	int competidorRandom=0;
	File folder = new File(patST+ "\\"+ folderg);
	File[] listOfFiles = folder.listFiles();

	File[] listofkeyboards;
	listofkeyboards=folder.listFiles();
	ArrayList <Integer> competidores = new ArrayList<Integer>();
	listofkeyboards=folder.listFiles();
ArrayList <Integer> Padres = new ArrayList<Integer>();
for (int comp=0;comp<lenghtTorneo; comp++ ) {
	competidor = aptitudes.get(comp+1);
	aptitudtorneo = aptitudes.get(comp);
	
	if ( aptitudtorneo<=competidor) {
		Padres.add(competidores.get(comp));
	}
	
	competidorRandom = (int)(Math.random()*numIndividuos+1);
	if (competidorRandom<=numIndividuos) {
		competidorRandom = competidorRandom-1;
	}
	competidores.add(competidorRandom);


	tecladoCompetir= patST+"\\"+folderg+"\\"+listofkeyboards[competidores.get(comp)].getName();
	//tecladoCompetir = tecladoCompetir;
contenidoArchivo= Ma.readFiletoString(tecladoCompetir);
aptitud = Double.parseDouble(contenidoArchivo);
aptitudes.add(aptitud);
}
for (int comp=0;comp<lenghtTorneo; comp++ ) {
	
}
aux = aptitudes.get(0);
aptitud=0.0;
contenidoArchivo="";


return Padres;
}
public ArrayList<Integer> cargaIndividuo (String pathPopoblacion, String folderg, File archivo) throws IOException{
	String content ="";
	String getArchivo ="";
	String elemento ="";

		
ArrayList<Integer>Individuo = new ArrayList<Integer>();

String [] contenidoArchivo=null;
getArchivo = archivo.getAbsolutePath();
content = Ma.readFiletoString(getArchivo);
contenidoArchivo = content.split(",");

for (int cont=0;cont<contenidoArchivo.length; cont++) {
	elemento = contenidoArchivo[cont].replaceAll("\\s", "");
	elemento = elemento.trim();
	elemento = elemento.replaceAll(" ", "");
	elemento = contenidoArchivo[cont].replaceAll("\\[", "").replaceAll("\\]","");
Individuo.add(Integer.parseInt(elemento.trim()));
}
	return Individuo;
}
public void creaIndividuos(String pathPoblacion,int numIndividuos, int Generaciones, int numGenes, String nameIndividuo, String folder, ArrayList<Integer>individuosal) throws IOException {
	int numArchivos=0;
	pathPoblacion = pathPoblacion+"\\"+folder;
	File folderpop = new File(pathPoblacion);
	File[] listOfFiles = folderpop.listFiles();
	numArchivos = listOfFiles.length;
	numIndividuos = numIndividuos-numArchivos;
	
	//for (File file : listOfFiles) {
	for (int i=0;i<numIndividuos; i++) {
		 nIndividuo+=1;
		for(int g=0; g<Generaciones; g++) {
			
		}
		for (int k=0; k<numGenes; k++) {
			int randomGen = (int) (Math.random()* numGenes+1);
			if (individuosal.size()==0) {
				individuosal.add(randomGen);
			}
			if (!individuosal.contains(randomGen)) {
				individuosal.add(randomGen);
				System.out.println("Inserte permutacion: " + randomGen);
			} else {
				k--;
			}if (k>=numGenes-2) {
				k=numGenes;
			}
		}
		nameIndividuo ="Individuo"+nIndividuo;
		Ma.rellenaIndividuos(pathPoblacion, individuosal, nameIndividuo, folder, Generaciones);
		individuosal.clear();
	}

}
public ArrayList<Integer> PasaIndividuos (String patPop, String pathEstats, ArrayList<Integer>Padres, String folderg, int numGeneracion, int numIndividuos, String folder) {
	numIndividuos = numIndividuos-1;
	String realGeneration="";
	realGeneration = folder;
	if (numGeneracion>1) {
		numGeneracion = numGeneracion -1;
		folderg ="Generacion"+numGeneracion;
		folderg ="Generacion"+numGeneracion;
	// numGeneracion = numGeneracion -1;
	}
	// folderg ="Generacion"+numGeneracion;
	ArrayList<Integer> Individuo = new ArrayList<Integer>();
	String content ="";
	String getIndividuo = "";
	String nombreIndividuo="";
	String elemento="";
	
	String [] contenidoIndividuo= null;
Manejo_archivos Mana = new Manejo_archivos();
	ArrayList <Double>aptitudes= new ArrayList<Double>();
ArrayList<Integer> padres = new ArrayList<Integer>();
	File folderpop = new File(patPop+ "\\"+ folderg);
	File[] listOfFiles = folderpop.listFiles();
	String tecladoCompetir;
	File[] listofindividuos;
	listofindividuos=folderpop.listFiles();
	for (int P= 0;P<1; P++) {
		if (Padres.size()>=1) {
			
		}
	getIndividuo= patPop+"\\"+folderg+"\\"+listofindividuos[Padres.get(P)].getName();
	try {
		content = Mana.readFiletoString(getIndividuo);
	   contenidoIndividuo = content.split(",");
		for (int cont=0;cont<contenidoIndividuo.length; cont++) {
			elemento = contenidoIndividuo[cont].replaceAll("\\s", "");
			elemento = elemento.trim();
			elemento = elemento.replaceAll(" ", "");
			elemento = contenidoIndividuo[cont].replaceAll("\\[", "").replaceAll("\\]","");
		Individuo.add(Integer.parseInt(elemento.trim()));
		}
		nombreIndividuo = listofindividuos[Padres.get(P)].getName();
	//	numGeneracion+=1;
		//folderg ="Generacion"+numGeneracion;
		nombreIndividuo = nombreIndividuo.replaceAll("Individuo", "Padre");
		
	//	Ma.creaDirectorio(patPop+ "\\"+folderg);
		
		//if (numGeneracion>=1) {
		//	Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folderg, this.numGeneracion);
		//	Ma.borraArchivo(patPop, folderg, Padres, numGeneracion);
	//	}
		//Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folderg, this.numGeneracion);

		if (numGeneracion>=1) {
			Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folder, this.numGeneracion);
			Ma.borraArchivo(patPop, folderg, Padres, numGeneracion);
		}
	} catch (IOException e) {
		 
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}
}
	return Individuo;
}
public void generaTeclados (ArrayList<Integer>Individuos, Map<String, Integer>TecladoMap, String pathPoblacion, String PathTecladoGenerado, String folder, Map<String, Integer> tablaIndividuo) throws IOException {
	String ruta = PathTecladoGenerado+"\\"+folder;
	Ma.creaCarpeta(ruta);
	System.out.println("El tamaño del individuo es: " + Individuos.size());

	numTeclado++;
	nombreTeclado = "teclado_Generado "+numTeclado;
	int filaAct=0;
	int numgen = 0;
	int numcolumna= 0;
	int numFilas=4;
String fileContent = "";
	boolean decimal = false;
	int numColumnas=40;
	int totalColumnas=10;
String contenidoFilas="";
	String[] contenidoFila = null;
int tamanioTeclado =0;
	String filasTeclado = "";
	double distancia = 0.0;
String []tecladoCompleto;
int numCromosoma = 0;
for (String clave:TecladoMap.keySet()) { 

	contenidoFila= clave.split(",");

	}

System.out.println("El numero de teclas del teclado es: "+tablaIndividuo.size());	
	for (String clavete:TecladoMap.keySet()) {
				while(numcolumna< contenidoFila.length) {
					
					for(Entry<String, Integer> entry: tablaIndividuo.entrySet()) {
						
							int numFila = TecladoMap.get(clavete);
							//System.out.println("Clave: " + clave + ", valor: " + numFila);
							contenidoFila= clavete.split(",");
				
				numgen = Individuos.get(numCromosoma); 
				if( numgen >=tablaIndividuo.size()){
					numgen = numgen-1;
				}
			      // if give value is equal to value from entry
			      // print the corresponding key 
				
			      if(entry.getValue() == numgen) {
			    	  numCromosoma+=1;
			    	  if (numCromosoma==1) {
			    		  fileContent+="[";
			    	  }
			    	  if (numCromosoma>=Individuos.size()) {
			    		  MA.writeTecladoAleatorio(fileContent, PathTecladoGenerado, nombreTeclado, folder);
			    			 return;
						
						   }
			    	 
			    	 numcolumna++;
			    	 
			    	 System.out.println("la tecla del gen " + numgen + " es " + entry.getKey());
				        fileContent+=","+entry.getKey();
			    	 if (numcolumna>= contenidoFila.length-1) {
			    		 filaAct++;
			    		 
			    		 System.out.println( "<------ El contenido de la fila "+ filaAct+ " fué "+ fileContent);
							MA.writeTecladoAleatorio(fileContent, PathTecladoGenerado, nombreTeclado, folder);
						  // System.out.println( "<------ El contenido de la fila "+ filaAct+ " fué "+ fileContent);
			    		 fileContent+="]";
							fileContent+="\n";
							numcolumna=0;
							   
							 //  fileContent+="]";
								//fileContent+="\n";
							   //filaAct++;
							 
						
							 //  numFila++;
			    	 }
								
			    		 //numFila++;
			    		 //numcolumna=0;
			    		 
			        
			        //break;
			      }
					  
			     
					
			}
			 
	
			
				
		}
		

			}

		}
public ArrayList<Integer> PasaIndividuos_rellena (String patPop, String pathEstats, ArrayList<Integer>Padres, String folderg, int numGeneracion, int numIndividuos) {
	numIndividuos = numIndividuos-1;
	if (numGeneracion>1) {
		numGeneracion = numGeneracion -1;
		folderg ="Generacion"+numGeneracion;
	// numGeneracion = numGeneracion -1;
	}
	// folderg ="Generacion"+numGeneracion;
	ArrayList<Integer> Individuo = new ArrayList<Integer>();
	String content ="";
	String getIndividuo = "";
	String nombreIndividuo="";
	String elemento="";
	
	String [] contenidoIndividuo= null;
Manejo_archivos Mana = new Manejo_archivos();
	ArrayList <Double>aptitudes= new ArrayList<Double>();
ArrayList<Integer> padres = new ArrayList<Integer>();
	File folder = new File(patPop+ "\\"+ folderg);
	File[] listOfFiles = folder.listFiles();
	String tecladoCompetir;
	File[] listofindividuos;
	listofindividuos=folder.listFiles();
	for (int P= 0;P<1; P++) {
		if (Padres.size()>=1) {
			
		}
	getIndividuo= patPop+"\\"+folderg+"\\"+listofindividuos[Padres.get(P)].getName();
	try {
		content = Mana.readFiletoString(getIndividuo);
	   contenidoIndividuo = content.split(",");
		for (int cont=0;cont<contenidoIndividuo.length; cont++) {
			elemento = contenidoIndividuo[cont].replaceAll("\\s", "");
			elemento = elemento.trim();
			elemento = elemento.replaceAll(" ", "");
			elemento = contenidoIndividuo[cont].replaceAll("\\[", "").replaceAll("\\]","");
		Individuo.add(Integer.parseInt(elemento.trim()));
		}
		nombreIndividuo = listofindividuos[Padres.get(P)].getName();
	//	numGeneracion+=1;
		//folderg ="Generacion"+numGeneracion;
		nombreIndividuo = nombreIndividuo.replaceAll("Individuo", "Padre");
		
	//	Ma.creaDirectorio(patPop+ "\\"+folderg);
		Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folderg, this.numGeneracion);
		if (numGeneracion>=0) {
		Ma.borraArchivo(patPop, folderg, Padres, numGeneracion);
		}
	} catch (IOException e) {
		 
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}
}
	return Individuo;
}
public boolean PasaIndividuos_rellena (String patPop, String pathEstats, ArrayList<Integer>Padres, String folderg, int numGeneracion, int numIndividuos, String folder, int contador) {
	numIndividuos = numIndividuos-2;
	boolean poblacionRellena = false;
	
	String realGeneration="";
	realGeneration = folder;
	if (numGeneracion>1) {
		numGeneracion = numGeneracion -1;
		folderg ="Generacion"+numGeneracion;
		folderg ="Generacion"+numGeneracion;
	// numGeneracion = numGeneracion -1;
	}
	// folderg ="Generacion"+numGeneracion;
	ArrayList<Integer> Individuo = new ArrayList<Integer>();
	String content ="";
	String getIndividuo = "";
	String nombreIndividuo="";
	String elemento="";
	int sizePopulation=0;
	
	String [] contenidoIndividuo= null;
Manejo_archivos Mana = new Manejo_archivos();

ArrayList<Integer> padres = new ArrayList<Integer>();
	File folderpop = new File(patPop+ "\\"+ folderg);
	File folderpopact = new File(patPop+ "\\"+ realGeneration);
	
	File[] listOfFiles = folderpop.listFiles();
	sizePopulation =folderpop.list().length;

	String tecladoCompetir;
	File[] listofindividuos;
	listofindividuos=folderpop.listFiles();
	for (int P= 0;P<1; P++) {
		if (Padres.size()>=1) {
			
		}
	getIndividuo= patPop+"\\"+folderg+"\\"+listofindividuos[Padres.get(P)].getName();
	try {
		content = Mana.readFiletoString(getIndividuo);
	   contenidoIndividuo = content.split(",");
		for (int cont=0;cont<contenidoIndividuo.length; cont++) {
			elemento = contenidoIndividuo[cont].replaceAll("\\s", "");
			elemento = elemento.trim();
			elemento = elemento.replaceAll(" ", "");
			elemento = contenidoIndividuo[cont].replaceAll("\\[", "").replaceAll("\\]","");
		Individuo.add(Integer.parseInt(elemento.trim()));
		}
		nombreIndividuo = listofindividuos[Padres.get(P)].getName();
	//	numGeneracion+=1;
		//folderg ="Generacion"+numGeneracion;
		nombreIndividuo = nombreIndividuo.replaceAll("Individuo", "Padre");
		
	//	Ma.creaDirectorio(patPop+ "\\"+folderg);
		
		//if (numGeneracion>=1) {
		//	Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folderg, this.numGeneracion);
		//	Ma.borraArchivo(patPop, folderg, Padres, numGeneracion);
	//	}
		//Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, folderg, this.numGeneracion);

		if (numGeneracion>=1) {
			Ma.writeIndividuosal(patPop, Individuo, nombreIndividuo, realGeneration, this.numGeneracion);
			
		}
	} catch (IOException e) {
		 
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}
	contador=folderpopact.list().length;
	//contador=folderpopact.list().length;
	System.out.println("el tamaño de la poblacion es: "+folderpopact.list().length);
	if(contador==numIndividuos) {
		poblacionRellena= true;
	}
}
	return poblacionRellena;
}
public ArrayList<Integer>SeleccionTorneo_OneParent(String pathTG, String folderg, int numIndividuos, String patpoblacion, int sizeTorneo, int numgeneracion, ArrayList<Integer>Padres) throws IOException {
	File folder = new File(pathTG+ "\\"+ folderg);


	File[] listOfAptitudes = folder.listFiles();
	String padreGanador ="";
	String content ="";
	int numCompetidor =0;
	String tecladoCompetir ="";
	if (!Padres.isEmpty()) {
		for (int i=0; i<Padres.size(); i++) {
		numCompetidor = Padres.get(i);
		
		tecladoCompetir= pathTG+"\\"+ folderg + "\\"+listOfAptitudes[numCompetidor-1].getName();
		}
		padreGanador = new String(Files.readAllBytes(Paths.get(tecladoCompetir.toString())));
		

	}
	numIndividuos = numIndividuos-1;
	int competidorRandom = 0;
	int numCompetidores=sizeTorneo;
	int numArchivo=0;
	int numTeclado=0;
	ArrayList<Integer> indiceArchivo =new ArrayList<Integer>();
	ArrayList<Integer> competidores =new ArrayList<Integer>();
	//while(!competidores.contains(competidorRandom)&&competidores.size()!=2) {
	for (int i=0;i<sizeTorneo;i++) {
		competidorRandom = (int)(Math.random()*numIndividuos+1);;
		competidores.add(competidorRandom);
	}
				
		//}
		
	
//	}

	String pathTeclados ="";
	File[] listofAptitudes = null;
	String tecladoAleatorio="";
	String []FileContent;
	double competidor=0.0;
	String palabra="";
	double aptitud = 0 ;

	int numGanadores=0;
	int padreAusente=0;
	int numCompetidorOriginal=0;
	double ganador = 0;
	double maxDist =0.0;
	double valueaptd=0.0;
	int indicePadre=0;
	int indiceinicial = 10;
	boolean poblacionCompleta = false;
	
	
	double padreWinner =0.0;
	ArrayList<Double>aptitudes = new ArrayList<Double>();
	
	ArrayList<Double>aptitudTeclados= new ArrayList<Double>();
	ArrayList<Integer> padres= new ArrayList<Integer>();

	for (int pa=0; pa<Padres.size(); pa++) {
	if (!Padres.isEmpty()) {
		indicePadre = Padres.get(pa);
		//indicePadre = indicePadre-1;
		
	// valida si ya existe un padre ganador
		if (padreWinner!=0) {
			
		}
	
	}
	}
	//bloque para recorrer los archivos del folder de aptitudes 
	//for (File file : listOfAptitudes) {


		listofAptitudes=folder.listFiles();
		
		for (File fileinfolder : listofAptitudes) {
			//if(numCompetidores>listofkeyboards.length) {
				
				if (fileinfolder.isFile()) {
					numArchivo++;
					tecladoAleatorio =fileinfolder.getAbsolutePath();
			    	nombreTeclado= fileinfolder.getName();
			    	content = new String(Files.readAllBytes(Paths.get(tecladoAleatorio)));
			    	aptitud = Double.valueOf(content);
			    	aptitudes.add(aptitud);
			    
			    	//Se termina de cargar las aptitudes de los teclados 
			    	
				}
				
		
			//recorrer archivos del folder
			
			//	Ma.listarArchivos(pathTG);
			
		
		
			
		}
		//Comienza el torneo entre aptitudes de teclados a competir 
 		for (int i=0;i<competidores.size(); i++) {
 			numCompetidorOriginal =competidores.get(i);
			//numTeclado++;
			
     		numCompetidor =competidores.get(i);
//if ( numgeneracion>1) {
			
			if (numCompetidor==10) {
			numCompetidor = numCompetidor-9;	
			}else {
			//numCompetidor = numCompetidor-1;
			}if (numCompetidor>10) {
				numCompetidor = numCompetidor-9;
			}else {
				if (numCompetidor<10) {
				if (numCompetidor==1) {
					numCompetidor= 10;
				}
					//numCompetidor =  numCompetidor + 9;
					tecladoCompetir= pathTG+"\\"+ folderg + "\\"+listofAptitudes[numCompetidor-1].getName();
		    		
					content = new String(Files.readAllBytes(Paths.get(tecladoCompetir.toString())));	
				}	
			}
			if (numCompetidor==10) {
				numCompetidor = numCompetidor-9;	
				}
			if (numCompetidor==1) {
				numCompetidor = 0;
			}
			
		//	}
// numCompetidor=numCompetidor- indiceinicial;
 else {
	 if (numCompetidor==1) {
		// numCompetidor = numCompetidor-10; 
	 }else {
	
		 
	 }
 }
// numCompetidor =13;
    		tecladoCompetir= pathTG+"\\"+ folderg + "\\"+listofAptitudes[numCompetidor-1].getName();
    		
    				content = new String(Files.readAllBytes(Paths.get(tecladoCompetir.toString())));
    				
    				aptitud = Double.valueOf(content);
    				aptitudTeclados.add(aptitud);
    				valueaptd = aptitudTeclados.get(0);
    				if (aptitudTeclados.size()>1) {
    					competidor= aptitudTeclados.get(i);
    				
    				if (valueaptd<=competidor) {
    					numCompetidor = competidores.get(0);
    					//padres.add(numCompetidor);
    					System.out.println("el valor del ganador es : "+valueaptd);
    					padres.add(numCompetidor);
    					//Padres.addAll(padres);
    					return padres;
    				}else {
    					
    						numCompetidor =competidores.get(i-1);
    						System.out.println("el valor del PADRE ganador es : "+valueaptd);
    						padres.add(numCompetidor);
        					Padres.addAll(padres);
        					return padres;
    						
    					
    				}
    				}
		}
		//Se termina de cargar todas las aptitudes de los teclados 
		
		//Comienza el torneo entre aptitudes de los teclados/individuos 
	


    	
	numTeclado=0;
	aptitudes.clear();
	aptitudTeclados.clear();
//}

	

	





	
	File[] file = folder.listFiles();
	//System.out.println("El nombre del folder es: ");
	if (padres.size()==1) {
		int padrefaltante = (int)(Math.random()*numIndividuos+1);
		padres.add(padrefaltante);
	}
	return padres;


}
public boolean cruzaEmparejamientoV2 (ArrayList<Integer>Padres, String pathPoblacion, int numGeneracion, String folderg, String pathst, int numGenes, int numindividuos, String folderactg, int numHijosParam) throws IOException {
int tamPop=0;
String pathPoblacionact ="";

//	String folderactg ="";
	String pathpopUp="";
	String pathpopSimple=pathPoblacion;
//	numGeneracion = numGeneracion+1;
//	folderactg = "Generacion"+numGeneracion;
	String ruta =pathPoblacionact =pathPoblacion+"\\"+folderactg;
    pathpopUp = pathPoblacion +"\\"+folderactg;
	pathpopUp = ruta;
	//ruta= pathPoblacion+"\\"+ folderactg;
numGenes = numGenes-3;
Ma.creaCarpeta(ruta);
pathPoblacion = pathPoblacion + "\\"+ folderg;
ArrayList<Integer> indiceArchivo =new ArrayList<Integer>();	
	cruza=false;
//this.pathpoblacionglobal = pathPoblacion+ "\\"+ folderactg;
String nombreindHijo="";
Ma.creaFolder(pathpoblacionglobal);

 numGeneracion = numGeneracion -1;
	ArrayList<Integer> Hijo1 = new ArrayList<Integer>();
	ArrayList<Integer> Hijos = new ArrayList<Integer>();
	ArrayList<Integer> Hijo2 = new ArrayList<Integer>();
	ArrayList<Integer> Padre1 = new ArrayList<Integer>();
	ArrayList<Integer> Padre2 = new ArrayList<Integer>();
	String content="";

	String pathaptitudes = pathst+ "\\"+folderg;
	String []contenidoIndividuo=null;
	int numPadre=0;
	int contadorh1=0;
	boolean poblacionCompleta= false;
	int contadorh2=0;
	int numrepetidosh1=0;
	int numrepetidosh2=0;
	int indicePadre=0;
	int contenidoPadres=0;
String getPadres="";
int permutacion=0;
int longitudIndividuo=40;
int numHijo = 0;
int permutacion2=0;
String getIndividuos="";
String caracter="";

	File folder = new File(pathPoblacion);
	//File folderpopact = new File(pathaptitudes);
	File folderpopact = new File(pathpopUp);
	File[] listOfFiles = folder.listFiles();
	File[] listofkeyboards = folderpopact.listFiles();
	for (File file : listOfFiles) {
		getIndividuos= file.getName();
		System.out.println("El nombre del folder es: "+file.getName());
		
		listofkeyboards=file.listFiles();
		for (File fileinfolder : listOfFiles) {
			if ( cruza==true) { 
				return poblacionCompleta;
			}
			System.out.println("El nombre del archivo es: "+fileinfolder.getName());
			if (fileinfolder.isFile()) {
			
			for(int i=0;i<Padres.size();i++) {
				numPadre++;
				indicePadre= Padres.get(i);
				getPadres =Manejo_archivos.recorreCarpeta(folder, indicePadre, folderg, listOfFiles, pathst, numindividuos, pathPoblacion);
	
			//	getIndividuos=file.getAbsolutePath()+"\\"+listOfFiles[indicePadre].getName();
				content = new String(Files.readAllBytes(Paths.get(getPadres)));
				
				contenidoIndividuo = content.split(",");
				System.out.println("El padre es :"+getIndividuos+" tiene una longitud de : "+content.length());
				for (int pa=0;pa<longitudIndividuo-1;pa++) {
					if (numPadre==1) {
					caracter = contenidoIndividuo[pa];
					caracter = caracter.trim();
	caracter = caracter.replaceAll("\\[", "").replaceAll("\\]","");
						permutacion = Integer.parseInt(caracter);
						Padre1.add(permutacion);
						
						}else if (numPadre==2) {
							caracter = contenidoIndividuo[pa];
							caracter = caracter.trim();
			caracter = caracter.replaceAll("\\[", "").replaceAll("\\]","");
								permutacion = Integer.parseInt(caracter);
								Padre2.add(permutacion);	
						}	
							}
			
							
					}
			int pCorte1 = (int)(Math.random()*numGenes+1);
			int pCorte2 = (int)(Math.random()*numGenes+ pCorte1);
			if (pCorte1==pCorte2) {
				pCorte2 = pCorte1+pCorte2;
				pCorte1 = pCorte1+5;
			}else if (pCorte2>numGenes) {
				pCorte2 = pCorte2-1;
				pCorte1 = pCorte2-pCorte1; 
				pCorte2 = numGenes;
				if (pCorte1>pCorte2) {
			pCorte1 = pCorte1 -pCorte2;
			if (pCorte1>numGenes) {
				pCorte1 = 40;
			}
				}
			if (pCorte2>numGenes) {
				pCorte2 =40;
			}
			}
			
			// Rellenar hijo 2 de acuerdo a puntos de corte
			contenidoPadres = pCorte2 - pCorte1;
			if (pCorte2>pCorte1) {
				pCorte1 = pCorte2;
			}
			for (int hi2=pCorte1; hi2<pCorte2; hi2++) {
	if (Hijo2.size()<longitudIndividuo) {
				Hijo2.add(Padre1.get(hi2)); 
	}
					
				
			
			}
			// Rellenar hijo 1 de acuerdo a puntos de corte
						contenidoPadres = pCorte2 - pCorte1;
						
						for (int hi1=pCorte1; hi1<pCorte2; hi1++) {
							//Hijo1.add(Padre2.get(hi1));
							//contador++;
						if (!Hijo2.contains(Padre1.get(hi1))){
							Hijo1.add(Padre1.get(hi1));
						}
						}
						// Rellenar los elementos faltantes en el hijo 2
				
Hijo2.addAll(Hijo1);
while (Hijo2.size()<longitudIndividuo-1) {

for (int i =0;i<longitudIndividuo-1; i++) {
	permutacion = Padre1.get(i);
	if (!Hijo1.contains(permutacion)){
		if (!Hijo2.contains(permutacion)){
			Hijo2.add(Padre1.get(i));	
		
		}else {
			//Hijo2.add(Padre2.get(i));	
		}
	}
}
//numHijo+=1;
		//contadorh1 = contadorh1+ contadorh2;
}
Hijo2g = Hijo2;
contadorh1=0;
numrepetidosh2=0;


while (Hijo1.size()<longitudIndividuo-1) {

	for (int i =0;i<longitudIndividuo-1; i++) {
		permutacion = Padre2.get(i);
		if (!Hijo1.contains(permutacion)){
			if (!Hijo2.contains(permutacion)){
				Hijo2.add(Padre2.get(i));	
			
			}else {
				Hijo1.add(Padre1.get(i));	
			}
		}
	}

}
Hijo1g = Hijo1;
cruza = true;
tamPop = folderpopact.listFiles().length;
if (tamPop==numindividuos) {
	poblacionCompleta= true;
	return poblacionCompleta;
}
			}
			tamPop = folderpopact.listFiles().length;
			if (tamPop==numindividuos) {
				poblacionCompleta= true;
				return poblacionCompleta;
			}
			//numPadre=0;
			numHijo = numHijosParam;
			nombreindHijo= "Hijo_"+numHijo;
			Ma.EscribeHijos(pathpopSimple, Hijo1, nombreindHijo, folderactg);
	
			int fileIndex = (int)(Math.random()*numindividuos+1);
			indiceArchivo.add(fileIndex);
			if (numGeneracion==0) {
			Ma.borraArchivo(pathPoblacion, folderactg, indiceArchivo, numGeneracion);
			}
			
			//Hijo1.clear();
		//	numHijo++;
		//	nombreindHijo= "Hijo_"+numHijo;
		//	Ma.EscribeHijos(pathPoblacion, Hijo2, nombreindHijo, folderactg);
			int fileId = (int)(Math.random()*numindividuos+1);
			indiceArchivo.add(fileId);
			if (numGeneracion==0) {
		//	Ma.borraArchivo(pathPoblacion, folderg, indiceArchivo, numGeneracion);
			}
			//mutacionHijos(Hijo1, Hijo2);
			//Hijo2.clear();
		//return;
			
		}
	}
	return poblacionCompleta;

}
}



		    	
		    
	    
			

	
		    	
		    
		   
		
	
	












