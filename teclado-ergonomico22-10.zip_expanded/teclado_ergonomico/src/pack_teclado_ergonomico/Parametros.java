package pack_teclado_ergonomico;
import librerias_componentes.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import librerias_componentes.*;
import librerias_componentes.IOBinFile;
import librerias_componentes.ManejoArchivos;
import librerias_componentes.MyListArgs;
import pack_teclado_ergonomico.*;
import pack_teclado_ergonomico.*;


@SuppressWarnings("unused")
public class Parametros {
	public static String getPATH_ENTRADA() {
	return PATH_ENTRADA;
}
public static void setPATH_ENTRADA(String pATH_ENTRADA) {
	PATH_ENTRADA = pATH_ENTRADA;
}
public static String getPATH_SALIDA() {
	return PATH_SALIDA;
}
public static void setPATH_SALIDA(String pATH_SALIDA) {
	PATH_SALIDA = pATH_SALIDA;
}
private String ConfigFile;
public static int NUM_MANOS;

public static String PATH_SALIDA;
public static String PATH_ENTRADA;
public static String TECLADOS_GENERADOS;






public static String getPATH_POP() {
	return PATH_POP;
}
public static void setPATH_POP(String pATH_POP) {
	PATH_POP = pATH_POP;
}
public static int getNUM_MANOS() {
	return NUM_MANOS;
}
public static void setNUM_MANOS(int nUM_MANOS) {
	NUM_MANOS = nUM_MANOS;
}
public static String getDEDOS_DISPONIBLES() {
	return DEDOS_DISPONIBLES;
}
public static void setDEDOS_DISPONIBLES(String dEDOS_DISPONIBLES) {
	DEDOS_DISPONIBLES = dEDOS_DISPONIBLES;
}
public static String DEDOS_DISPONIBLES;
public static String IDIOMA;
public static String TECLADO_OPTIMIZAR;
public static String MANO_UTILIZAR;
public static String DATASET;
public static String PATH_POP;
public static String PATH_ESTADISTICAS;

public static String getIdioma() {
	return IDIOMA;
}
public static void setIdioma(String idioma) {
	
}
MyListArgs Param;
IOBinFile BinFile;
ManejoArchivos IO;
public Parametros(String []Args) {

	IO = new ManejoArchivos();
    BinFile = new IOBinFile();

    Param = new MyListArgs(Args);
	ConfigFile = Param.ValueArgsAsString("-CONFIG", "");//Archivo donde se especifican los parametros necesarios para este componente
    if (!ConfigFile.equals("")) {
        Param.AddArgsFromFile(ConfigFile);
        //System.out.println("Entre");
    }
    NUM_MANOS       = Param.ValueArgsAsInteger   ("-NUM_MANOS"   , 0    );   //Ruta de trabajo
	
	DEDOS_DISPONIBLES	= Param.ValueArgsAsString("-DEDOS_DISPONIBLES", ""); //Se expecifica el tama�o de grama "N" 
	TECLADO_OPTIMIZAR = Param.ValueArgsAsString("-TECLADO_OPTIMIZAR", "");
	IDIOMA	= Param.ValueArgsAsString("-IDIOMA", ""); //Se expecifica el tama�o de grama "N"
	MANO_UTILIZAR	= Param.ValueArgsAsString("-MANO_UTILIZAR", "");
	DATASET	= Param.ValueArgsAsString("-DATASET", "");
	PATH_POP       = Param.ValueArgsAsString   ("-PATH_POP"   , ""    ); 
	PATH_ESTADISTICAS      = Param.ValueArgsAsString   ("-PATH_ESTADISTICAS"   , ""    ); 
	TECLADOS_GENERADOS	= Param.ValueArgsAsString("-TECLADOS_GENERADOS", "");
	TECLADO_OPTIMIZAR  = Param.ValueArgsAsString("-TECLADO_OPTIMIZAR", "");
}



public static String getTECLADOS_GENERADOS() {
	return TECLADOS_GENERADOS;
}
public static void setTECLADOS_GENERADOS(String tECLADOS_GENERADOS) {
	TECLADOS_GENERADOS = tECLADOS_GENERADOS;
}
public static String getPATH_ESTADISTICAS() {
	return PATH_ESTADISTICAS;
}
public static void setPATH_ESTADISTICAS(String pATH_ESTADISTICAS) {
	PATH_ESTADISTICAS = pATH_ESTADISTICAS;
}
public static String getDATASET() {
	return DATASET;
}
public static void setDATASET(String dATASET) {
	DATASET = dATASET;
}
public static String getMANO_UTILIZAR() {
	return MANO_UTILIZAR;
}
public static void setMANO_UTILIZAR(String mANO_UTILIZAR) {
	MANO_UTILIZAR = mANO_UTILIZAR;
}
public static String getTECLADO_OPTIMIZAR() {
	return TECLADO_OPTIMIZAR;
}
public static void setTECLADO_OPTIMIZAR(String tECLADO_OPTIMIZAR) {
	TECLADO_OPTIMIZAR = tECLADO_OPTIMIZAR;
}
}
