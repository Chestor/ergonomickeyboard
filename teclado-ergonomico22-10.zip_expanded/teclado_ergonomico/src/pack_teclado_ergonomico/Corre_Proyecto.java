package pack_teclado_ergonomico;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Corre_Proyecto {

estadisticas_texto etext = new estadisticas_texto();
Manejo_archivos ma = new Manejo_archivos();
private String textFile;
Map<String, Integer> handStatistics = new HashMap<String, Integer>();
	Map<Integer, Double> fingerPorcentageMD = new HashMap<Integer, Double>();
	Map<String, Integer> fingerStatisticsMD = new HashMap<String, Integer>();
	Map<String, Integer> fingerStatisticsMI = new HashMap<String, Integer>();
	Map<String, Integer>textStatistics = new HashMap<String, Integer>();
	Map<String, Integer>tecnicaDigitacionMD = new LinkedHashMap<String, Integer> ();
	Map<String, Integer>tecnicaDigitacionMI = new LinkedHashMap<String, Integer> ();
	Map<String, Integer> teclasPulsadasMD = new LinkedHashMap<String, Integer>();
	Map<String, Integer> teclasPulsadasBH = new HashMap<String, Integer>();
	Map<String, Integer> teclasPulsadasMI = new LinkedHashMap<String, Integer>();
	Map<Integer, Integer> Individuos = new HashMap<Integer, Integer>();
	private Map<String, Integer>tablaIndividuo = new LinkedHashMap<String, Integer>();
	Map<String, Integer> teclasPulsadasLH = new HashMap<String, Integer>();
	ArrayList<String>Teclado;
	Map<String, Integer>tecladoMap = new LinkedHashMap<String, Integer>();
	
	String PATH;
	private String texto;
	String pathEstadisticas;
	int numManos;
	int dedosDisponibles;
	String teclado_toOptimizar;
    criteriosUsoTeclado cut = new criteriosUsoTeclado();
	Estructura_teclado ETE = new Estructura_teclado();
	GeneraPoblacion GP = new GeneraPoblacion();
	CalculaDistancia CD = new CalculaDistancia();
	generaTeclados GT = new generaTeclados();
	
private String FileContent;
public void Ejecuta_proyecto ( String PATH, String teclado_toOptimizar, String Path_tecladoGenerado, int numManos, String dedosDisponibles, String idioma, String mano_utilizar, String dataset, String pathPoblacion, String pathEstadisticas) throws IOException {
	//PATH, teclado_toOptimizar, Path_tecladoGenerado, numManos, dedosDisponibles, idioma, mano_utilizar, dataset,pathPoblacion, pathEstadisticas
System.out.println(PATH);
	

	
	FileContent =ma.readFiletoString(dataset);
	tecnicaDigitacionMD = etext.tecnica_Digitacion_MD();
	tecnicaDigitacionMI = etext.tecnica_DigitacionMI();
	

	
	/// Inicia bloque para cargar estadisticas //
	textStatistics = etext.estadistica_texto(FileContent);
	tecladoMap =etext.cargaTecladoOptimizar( teclado_toOptimizar);
	tablaIndividuo= GP.AsignaTeclas(); 
	fingerStatisticsMD = etext.putEstadisticasMD(tecnicaDigitacionMD, FileContent, tecladoMap);

		fingerStatisticsMI =etext.putEstadisticasMI(FileContent, tecnicaDigitacionMI, tecladoMap);
	   
		//GP.genera_poblacion_Teclados(pathPoblacion, path_tecladoGenerado, tecladoMap);
		//etext.lee(pathPoblacion, path_tecladoGenerado, tecladoMap);
 		teclasPulsadasMD=	etext.put_pulsacionesDedosMD(tecnicaDigitacionMD, FileContent, tecladoMap);
		teclasPulsadasMI=	etext.put_pulsacionesDedosMI(tecnicaDigitacionMI, FileContent, tecladoMap);
		// termina bloque para cargar estadísticas //
		GP.getvarET(FileContent, Path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMD, tecnicaDigitacionMI, pathEstadisticas, teclasPulsadasMD, teclasPulsadasMI, teclasPulsadasMD, dedosDisponibles, mano_utilizar, numManos);
		GP.genera_poblacion_Teclados(pathPoblacion, Path_tecladoGenerado, tecladoMap, FileContent, tablaIndividuo, teclasPulsadasMI);
		
		if (numManos==1&&mano_utilizar.contains("DERECHA")) {
			
			
			
			//String FileContent, String Path_tecladoGenerado, Map<String, Integer> tecladoMap, Map<String, Integer> textStatistics, Map<String, Integer>tecnicaDigitacionMD, Map<String, Integer>tecnicaDigitacionMI, String pathEstatistics, Map<String, Integer> teclasPulsadasMD, Map<String, Integer> fingerStatisticsMD, Map<String, Integer> teclasPulsadasMI, String dedosDisponibles, String mano_utilizar
			etext.evaluaTecladosAleatorios( texto, Path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMI,tecnicaDigitacionMD, teclasPulsadasMD, teclasPulsadasLH, pathEstadisticas, numManos, dedosDisponibles, mano_utilizar, teclasPulsadasLH);
			//String texto, String pathTG,Map<String, Integer> teclado, Map<String, Integer> textStatistics, Map<String, Integer>tecMD, Map<String, Integer> tecMI,Map<String, Integer> fingerMD,Map<String, Integer> teclasPulsadasBH, String pathEstadisticas, int numManos, int dedosDisponibles, String mano_utilizar
		}else if (numManos==1&&mano_utilizar.contains("IZQUIERDA")) {
			etext.evaluaTecladosAleatorios( texto, Path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMI,tecnicaDigitacionMD, teclasPulsadasMD, teclasPulsadasLH, pathEstadisticas, numManos, dedosDisponibles, mano_utilizar, teclasPulsadasLH);	
		}else if (numManos>1) {
			etext.evaluaTecladosAleatorios( texto, Path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMI,tecnicaDigitacionMD, teclasPulsadasMD, teclasPulsadasLH, pathEstadisticas, numManos, dedosDisponibles, mano_utilizar, teclasPulsadasLH);
	textStatistics = etext.estadistica_texto(FileContent);
	//GP.genera_poblacion_Teclados(pathPoblacion, path_tecladoGenerado, tecladoMap);
//GP.getvarET(FileContent, path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMD, tecnicaDigitacionMI, teclasPulsadasMD, teclasPulsadasMI);
	//FileContent, path_tecladoGenerado, tecladoMap, textStatistics, tecnicaDigitacionMD, tecnicaDigitacionMI, teclasPulsadasMD, teclasPulsadasMI
	tecladoMap =etext.cargaTecladoOptimizar(teclado_toOptimizar);
	etext.putEstadisticasMD(tecnicaDigitacionMD, Path_tecladoGenerado, tecladoMap);
	etext.putEstadisticasMI(FileContent,tecnicaDigitacionMI,  tecladoMap);
	//GA.genera_poblacion(pathPoblacion, path_teclagoGenerado, tecladoMap);
	fingerStatisticsMD=	etext.put_pulsacionesDedosMD(tecnicaDigitacionMD, FileContent, tecladoMap);
	fingerStatisticsMI=	etext.put_pulsacionesDedosMI(tecnicaDigitacionMI, FileContent, tecladoMap);
		}

	
		

		
	
}

	
}




