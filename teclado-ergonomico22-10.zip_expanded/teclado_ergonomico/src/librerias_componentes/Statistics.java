/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package librerias_componentes;

import java.util.Arrays;

/**
 *
 * @author MACSCO12
 */


public class Statistics 
{
    static double[] data;
    static int size;   

    public Statistics(double[] data) 
    {
        Statistics.data = data;
        size = data.length;
    }   

    public double getMean()
    {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/size;
    }

    double getVariance()
    {
        double mean = getMean();
        double temp = 0;
        for(double a :data)
            temp += (a-mean)*(a-mean);
        return temp/size;
    }

    public double getStdDev()
    {
        return Math.sqrt(getVariance());
    }

    public double median() 
    {
       Arrays.sort(data);

       if (data.length % 2 == 0) 
       {
          return (data[(data.length / 2) - 1] + data[data.length / 2]) / 2.0;
       } 
       return data[data.length / 2];
    }
    
    public double sumAll(){
        double s=0.0;
        for (int i = 0; i < data.length; i++) {
            s+=data[i];
        }
        return s;
    }
    
}

