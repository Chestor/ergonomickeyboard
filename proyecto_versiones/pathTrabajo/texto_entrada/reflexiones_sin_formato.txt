Sólo reflexiones que nacen en el día a día…

Odio, en realidad, detesto escribir sobre el humo blanquecino con olor a madrugada que se desprende de labios trasnochados, o del olor a alcohol que se escapa en alguna callejuela abandonada. No quiero escribir de los letreros grises, del hastío y de la pesadez que arrastra el mundo. 

Necesito escribir de cosas simples, de la vida y sus trivialidades, de la armonía con la que cae el rocío en una hoja en brote, de la voz de un niño pidiendo alimento, de como se da la lucha en una juventud con poca esperanza.

 

09 de abril del 2018

Es increíble, cada vez que escribo algo en este trozo de mí me doy cuenta del tiempo, como corre tan de prisa, cómo nos marca, como casi, sólo casi, parece real.

A medida que los sucesos han ido cambiando, tal vez expandiéndose por otros caminos algo me ata a este pequeño rincón que me vio partir cuando quise abrirme un poco al mundo. No puedo cerrarlo, ni quiero…, ahora todo camina más a prisa, abarcando otras fronteras, haciéndome menos mía y más de todos, y aún así, acá estoy. Perdida por estas letras, navegando como si no hubiera pasado nada. Y ha pasado tanto…

Hoy salí a caminar, como cada día, por un pequeño sendero que me gusta por lo poco escarpado, los muchos árboles, el pequeño arroyo. La luz se filtraba por las ramas, las hormigas ya salieron después de la última lluvia. Por fin llega la primavera, llevo casi cuatro años de invierno, al cambiar de país ha coincidido que me ha tocado estar en los inviernos de cada uno de los países en que he estado pasando temporadas y añoro el sol, vestirme con ropa liviana, dedicarme a mis plantas, verlas felices, amadas al tener el calor y agua que necesitan para abrirse al mundo a experimentar la existencia en dicha forma.

En un momento me detuve a descansar bajo uno de los pinos, uno que tenía las ramas tan bajas que podían rozar mi cabeza, y me dejé llevar. Cerré los ojos, me concentré en la respiración y sin mayor esfuerzo se hizo el silencio. Estaba el árbol y yo, cada uno perdiendo lentamente su forma para ser sólo esa gran energía vibrando en cada una de las formas, yo y el árbol, pero con una sola fuente. La que nos da la forma. Cuánta enseñanza!!, su ramaje se movía con el viento sin oponer la menor resistencia, y el tronco tan firme que podrías colgar una casa en él, y el ramaje se movía tan liviano, sin oponer la menor resistencia a lo que viniera, sólo dejarse llevar.

14 de Mayo 2016