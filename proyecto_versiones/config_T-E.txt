#Rutas de trabajo para el proyecto
-PATH_TRABAJO 	"C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\\"
-TECLADO_OPTIMIZAR	"C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\Teclado_Optimizar\teclado qwerty.txt"
-TECLADOS_GENERADOS	"C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\tecladosGenerados"	
-PATH_SALIDA  C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\Teclado_Optimizado

-DATASET	"C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\texto_entrada\texto_prueba.txt"
-PATH_POP "C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\Poblacion"
-PATH_ESTADISTICAS	"C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\estadisticasTeclados"

# correr componente

-CONFIG  "C:\Users\Chestor\Documents\proyecto_versiones\config_T-E.txt"

#apartado paea definir lo criterios con los que desea utilizar el teclado, en caso de utilizar una mano especificar "izquierda" o "derecha", en caso de utilizar ambas manos no escribir nada, cada dedo debe ser identificado por una letra de la a a la e comenzando por el dedo pulgar, b índice, c medio, d anular y e meñique.

# definir criterios con los que desea utilizar el teclado 

##criterios ergonómicos

-NUM_MANOS 1
-DEDOS_DISPONIBLES b,c,d,e
-IDIOMA "español"
-DIR_Movimiento
-MANO_UTILIZAR "IZQUIERDA"

# parámetros para correr el componente de población 

-PATH_POP "C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\Poblacion"


java -jar C:\Users\Chestor\Documents\proyecto_versiones\pathTrabajo\JARS\generaPoblacion.jar -CONFIG 


